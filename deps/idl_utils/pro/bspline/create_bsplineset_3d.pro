function create_bsplineset_3d, fullbkpt, nord, npoly=npoly, n2poly=n2poly

      numbkpt = n_elements(fullbkpt)
      numcoeff = numbkpt - nord

      if (NOT keyword_set(npoly)) then begin
         sset = $
         { fullbkpt: fullbkpt             , $
           bkmask  : bytarr(numbkpt) + 1  , $
           nord    : long(nord)           , $
           coeff   : fltarr(numcoeff)     , $
           icoeff  : fltarr(numcoeff) }
      endif else begin
         if(NOT keyword_set(n2poly)) then begin
         sset = $
         { fullbkpt: fullbkpt            , $
           bkmask  : bytarr(numbkpt) + 1 , $
           nord    : long(nord)          , $
           xmin    : 0.0                 , $
           xmax    : 1.0                 , $
           funcname: 'legendre'          , $
           npoly   : long(npoly)         , $
           coeff   : fltarr(npoly,numcoeff) , $
           icoeff  : fltarr(npoly,numcoeff) }
         endif else begin
         sset = $
         { fullbkpt: fullbkpt            , $
           bkmask  : bytarr(numbkpt) + 1 , $
           nord    : long(nord)          , $
           xmin    : 0.0                 , $
           xmax    : 1.0                 , $
           ymin    : 0.0                 , $
           ymax    : 1.0                 , $
           funcname: 'poly2d'          , $
           npoly   : long(npoly)         , $
           npoly2   : long(n2poly)         , $
           coeff   : fltarr(npoly,n2poly,numcoeff) , $
           icoeff  : fltarr(npoly,n2poly,numcoeff) }
         endelse
      endelse

      return, sset
end

