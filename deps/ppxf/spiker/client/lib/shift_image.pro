function shift_image,im,dx0,dy0
;+
; NAME:        
;	 SHIFT_IMAGE
; PURPOSE:      Remap image by linear interpolation
; CATEGORY:
; CALLING SEQUENCE:
;       In = shift_image(im,dx,dy)
; INPUTS:
;       Im	= Image to be shifted
;	Dx	= Shift in X-direction
;	Dy	= Shift in Y-direction
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;       In
; COMMON BLOCKS:
;       None.
; SIDE EFFECTS:
;
; RESTRICTIONS:
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;       Z. Yi, UiO, June, 1992.
;	modified for MPFS-data by Victor Afanasiev, Jul 1999
;       modified in order to save memory by chil, 07/Mar/2006
;-
on_error,2
 ax=0 & ay=0
 dx=dx0 & dy=dy0
in=shift(im,rfix(dx),rfix(dy))

dx=dx-rfix(dx)	&	dy=dy-rfix(dy)
if((dx lt 0) and (dy lt 0)) then in=in+DX*(in -shift(in,-1,0))+DY*(in -shift(in,0,-1))
if((dx ge 0) and (dy lt 0)) then in=in+DX*(shift(in,1,0)-in)  +DY*(in -shift(in,0,-1))
if((dx lt 0) and (dy ge 0)) then in=in+DX*(in -shift(in,-1,0))+DY*(shift(in,0,1)-in)
if((dx ge 0) and (dy ge 0)) then in=in+DX*(shift(in,1,0)-in)  +DY*(shift(in,0,1)-in)

return,IN
END
