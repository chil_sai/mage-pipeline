function DISP_FAMILY
;+
; NAME:
;	DISP_FAMILY
; PURPOSE:
;	Return the current operating system as in !VERSION.OS_FAMILY 
;
; CALLING SEQUENCE
;	result = DISP_FAMILY()
; INPUTS: 
;	None
; OUTPUTS:
;	result - scalar string containing one of the four values
;		'WIN','MAC','VMS' or 'X'
; NOTES:
;	OS_FAMILY is assumed to be 'unix' if !VERSION.OS is not 'windows',
;		'MacOS' or 'vms'
;
;	To make procedures from IDL V4.0 and later compatibile with earlier
;	versions of IDL, replace calls to !VERSION.OS_FAMILY with OS_FAMILY().	
;
; PROCEDURES CALLED
;	function tag_exists
; REVISION HISTORY:
;	Written,  W. Landsman     
;-
 if tag_exist(!VERSION, 'DISP_FAMILY') then return, !VERSION.DISP_FAMILY

 case !VERSION.OS of

'windows': return, 'WIN'
  'Win32': return, 'WIN'
  'MacOS': return, 'MAC'
    'vms': return, 'VMS'
     else: return, 'X'

 endcase

 end
