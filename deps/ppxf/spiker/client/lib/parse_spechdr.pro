;-------------------------------------
; parsing spectral WCS
;-------------------------------------

pro parse_spechdr,spheader,wl=wl,nlam=nlam,islog=islog,velscale=velscale
islog=0
velScale=-1.0
nlam=sxpar(spheader,'NAXIS1')
bintype=strcompress(sxpar(spheader,'BINTYPE'),/remove_all)
crpix=sxpar(spheader,'CRPIX1',count=cntcrpix)
if(cntcrpix ne 1) then crpix=1.0
w0=sxpar(spheader,'CRVAL1')
disp=sxpar(spheader,'CDELT1')
if(disp eq 0.0) then disp=sxpar(spheader,'CD1_1')
ctype1=strcompress(string(sxpar(spheader,'CTYPE1')),/remove_all)
log10wl=0
if(string(sxpar(spheader,'WFITTYPE')) eq 'LOG-LINEAR') then begin ;; for SDSS
    log10wl=1
    ctype1='WAVE-LOG'
endif
if((ctype1 eq 'AWAV-LOG') or (ctype1 eq 'WAVE-LOG') or (ctype1 eq 'WAVE-ALOG')) then begin
    wl=(log10wl eq 1)? 10d^(w0+disp*(dindgen(nlam)-crpix+1.0)) : exp(w0+disp*(dindgen(nlam)-crpix+1.0))
    velScale=disp*299792.458d
    if(log10wl eq 1) then velScale=velScale*alog(10)
    islog=1
endif else begin
    if(bintype ne 'log') then begin
        wl=w0+disp*(dindgen(nlam)-crpix+1.0)
    endif else begin
        wl=w0*exp(disp*(dindgen(nlam)-crpix+1.0)/299792.458d)
        velScale=disp
        islog=1
    endelse
endelse
end

