function wlvac2atm,wlvac,inverse=inverse
    k=(1.0d + 2.735182d-4 + 131.4182/wlvac^2 + 2.76249d+8/wlvac^4)
    return, keyword_set(inverse) ? wlvac/k : wlvac*k
end
