;;; convolving spectra with Gauss-Hermite LOSVD in pixel or Fourier space
function convol_spec,spec,pars,factor=factor,fourier=fourier

; pars = [vel,sigma,h3,h4,h5,h6]    ; Velocities in pixels
if(n_elements(factor) ne 1) then factor=1d

s = size(spec)
nwl = s[1]
nsp2=2l^ceil(alog(nwl)/alog(2d))
dx = (keyword_set(fourier))? nsp2/2l : ceil(abs(pars[0])+5*pars[1]) ; Sample the Gaussian and GH at least to vel+5*sigma

losvd=gauss_hermite(pars,dx,fourier=fourier)

outspec=spec*0.0
ntemp = (s[0] eq 1)? 1 : product(s[2:s[0]]) ; Number of template spectra

if(s[0] lt 3) then begin
    for j=0,ntemp-1 do begin
        if(nsp2 gt nwl and ~keyword_set(fourier)) then begin
            spectmp=dblarr(nsp2)
            spectmp[0:nwl-1]=spec[*,j]
            spectmp[nwl:*]=spectmp[nwl-1]+$
                dindgen(nsp2-nwl)*(spectmp[0]-spectmp[nwl-1])/(nsp2-nwl)
        endif else spectmp=spec[*,j]
        outspectmp = (keyword_set(fourier))? $
            convol_1d_fourier(spectmp,losvd) : $
            convol(spectmp,losvd,/EDGE_TRUNCATE)
        outspec[*,j] = outspectmp[0:n_elements(spec[*,j])-1]
    endfor
endif else begin
    if(s[0] lt 4) then s[4]=1
    for jx=0,s[2]-1 do begin
        for jy=0,s[3]-1 do begin
            for jz=0,s[4]-1 do begin
                if(nsp2 gt nwl and ~keyword_set(fourier)) then begin
                    spectmp=dblarr(nsp2)
                    spectmp[0:nwl-1]=spec[*,jx,jy,jz]
                    spectmp[nwl:*]=spectmp[nwl-1]+$
                        dindgen(nsp2-nwl)*(spectmp[0]-spectmp[nwl-1])/(nsp2-nwl)
                endif else spectmp=spec[*,jx,jy,jz]
                outspectmp = (keyword_set(fourier))? $
                    convol_1d_fourier(spectmp,losvd) : $
                    convol(spectmp,losvd,/EDGE_TRUNCATE)
                outspec[*,jx,jy,jz] = outspectmp[0:n_elements(spec[*,jx,jy,jz])-1]
            endfor
        endfor
    endfor
endelse

return,outspec
end

