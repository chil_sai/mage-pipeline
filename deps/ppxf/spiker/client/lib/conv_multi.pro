function conv_multi,templates,$
  velvec=velvec, disvec=disvec, $                        ;; real "vecs"
  h3vec=h3vec, h4vec=h4vec, h5vec=h5vec, h6vec=h6vec, $
  nsegments=nsegments,velScale=velScale, pixconv=pixconv

    if(not keyword_set(velScale)) then velScale=40d
    if(not keyword_set(velvec)) then message,'Velvec is compulsory'
    if(not keyword_set(disvec)) then message,'Disvec is compulsory'
    if(n_elements(velvec) ne n_elements(disvec)) then $
        message,'Velvec and Disvec must be the same size'
    if(not keyword_set(h3vec)) then h3vec=velvec*0.0
    if(not keyword_set(h4vec)) then h4vec=velvec*0.0
    if(not keyword_set(h5vec)) then h5vec=velvec*0.0
    if(not keyword_set(h6vec)) then h6vec=velvec*0.0
    if(n_elements(nsegments) ne 1) then nsegments=5

    s_temp=size(templates)
    l_temp=s_temp[1]
    s_vec=size(velvec)
    l_vec=s_vec[1]
    l_min=(l_temp le l_vec)? l_temp : l_vec
    x_val=((0.5+findgen(nsegments))/nsegments)*l_min
    l_seg=long(x_val[1]-x_val[0])
    ;templates1=templates
    pars=dblarr(6,nsegments)
    tempconv_n=dblarr([s_temp[1:s_temp[0]],nsegments])
    
    for i=0,nsegments-1 do begin
        idxmin=(i eq 0)? 0L : floor(l_seg*(i-0.5))
        idxmax=(i lt nsegments-1)? floor(l_seg*(0.5+i))+l_seg-1L : s_temp[1]-1L
        pars[0,i]=velvec[x_val[i]]/velScale
        pars[1,i]=disvec[x_val[i]]/velScale
        pars[2,i]=h3vec[x_val[i]]
        pars[3,i]=h4vec[x_val[i]]
        pars[4,i]=h5vec[x_val[i]]
        pars[5,i]=h6vec[x_val[i]]
        if (s_temp[0] eq 4) then begin
            tempconv_n[idxmin:idxmax,*,*,*,i]=convol_spec(templates[idxmin:idxmax,*,*,*],pars[*,i],fourier=~keyword_set(pixconv))
        endif else if (s_temp[0] eq 3) then begin
            tempconv_n[idxmin:idxmax,*,*,i]=convol_spec(templates[idxmin:idxmax,*,*],pars[*,i],fourier=~keyword_set(pixconv))
        endif else if (s_temp[0] eq 2) then begin
            tempconv_n[idxmin:idxmax,*,i]=convol_spec(templates[idxmin:idxmax,*],pars[*,i],fourier=~keyword_set(pixconv))
        endif else begin
            tempconv_n[idxmin:idxmax,i]=convol_spec(templates[idxmin:idxmax],pars[*,i],fourier=~keyword_set(pixconv))
        endelse
    endfor

    if (s_temp[0] eq 4) then begin
        weight1=rebin(findgen(l_seg)/l_seg,l_seg,s_temp[2],s_temp[3],s_temp[4])
        weight2=1.0-weight1
        templates1=tempconv_n[*,*,*,*,0]
        for i=0,nsegments-2 do begin
            idxmin=floor(l_seg*(0.5+i))
            templates1[idxmin:idxmin+l_seg-1,*,*,*]=$
                weight1*tempconv_n[idxmin:idxmin+l_seg-1,*,*,*,i]+$
                weight2*tempconv_n[idxmin:idxmin+l_seg-1,*,*,*,i+1]
        endfor
        templates1[l_seg*(nsegments-0.5):*,*,*,*]=$
            tempconv_n[l_seg*(nsegments-0.5):*,*,*,*,nsegments-1]
    endif else if (s_temp[0] eq 3) then begin
        weight1=rebin(findgen(l_seg)/l_seg,l_seg,s_temp[2],s_temp[3])
        weight2=1.0-weight1
        templates1=tempconv_n[*,*,*,0]
        for i=0,nsegments-2 do begin
            idxmin=floor(l_seg*(0.5+i))
            templates1[idxmin:idxmin+l_seg-1,*,*]=$
                weight1*tempconv_n[idxmin:idxmin+l_seg-1,*,*,i]+$
                weight2*tempconv_n[idxmin:idxmin+l_seg-1,*,*,i+1]
        endfor
        templates1[l_seg*(nsegments-0.5):*,*,*]=$
            tempconv_n[l_seg*(nsegments-0.5):*,*,*,nsegments-1]
    endif else if (s_temp[0] eq 2) then begin
        weight1=rebin(findgen(l_seg)/l_seg,l_seg,n_elements(templates[0,*]))
        weight2=1.0-weight1
        templates1=tempconv_n[*,*,0]
        for i=0,nsegments-2 do begin
            idxmin=floor(l_seg*(0.5+i))
            templates1[idxmin:idxmin+l_seg-1,*]=$
                weight2*tempconv_n[idxmin:idxmin+l_seg-1,*,i]+$
                weight1*tempconv_n[idxmin:idxmin+l_seg-1,*,i+1]
        endfor
        templates1[l_seg*(nsegments-0.5):*,*]=$
            tempconv_n[l_seg*(nsegments-0.5):*,*,nsegments-1]
    endif else begin
        n_temp=1
        weight1=fltarr(l_seg,n_temp)
        for i=0,n_temp-1 do weight1[*,i]=findgen(l_seg)/double(l_seg)
        weight2=1.0-weight1
        templates1=tempconv_n[*,0]
        for i=0,nsegments-2 do begin
            idxmin=floor(l_seg*(0.5+i))
            templates1[idxmin:idxmin+l_seg-1]=$
                weight2*tempconv_n[idxmin:idxmin+l_seg-1,i]+$
                weight1*tempconv_n[idxmin:idxmin+l_seg-1,i+1]
        endfor
        templates1[l_seg*(nsegments-0.5):*]=$
            tempconv_n[l_seg*(nsegments-0.5):*,nsegments-1]
    endelse

    return, templates1
end
