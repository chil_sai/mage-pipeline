;;;;;;
; INPUT
;   pars -- [v/velScale, sigma/velScale, h3, h4, h5, h6]  h3..h6 are optional
;   n2 -- the half-width of the kernel. 
;         The output will be 2*n2+1 elements long if "/fourier" not set or 2*n2 otherwise
;   if n2 is not provided, it defaults to nsig*sigma (pars[1]) for pixel space kernel and 1024 for Fourier space kernel
;   nsig=nsig -- an optional parameter of number of sigma to sample the profile in the pixel space; defaults to 5
;   noneg=noneg -- a keyword to specify that no negative values should be returned, ignored if /fourier is set
;   fourier=fourier -- return a Fourier image of the kernel (dcomplex type) rather than the pixel space image
;
; OUTPUT
;   return value: gauss_hermite profile or its Fourier image
;
;;;;;;

function gauss_hermite,pars,n2,nonorm=nonorm,noneg=noneg,nsig=nsig,fourier=fourier,x=x
    if(n_elements(nsig) ne 1) then nsig=5d
    if(n_elements(factor) ne 1) then factor=1d
    if(n_params() eq 1) then n2 = (keyword_set(fourier))? 1024l : ceil(abs(pars[0])+ nsig*pars[1]) ; Sample the Gaussian and GH at least to vel+5*sigma

    npars = n_elements(pars)

    if(keyword_set(fourier)) then begin
        n_tf=2l*n2
        w_f=2d*!dpi*(dindgen(n_tf)-(n2-1l))/n_tf
        if(arg_present(x)) then x=w_f
        w_f1=pars[1]*w_f
        w_f2=w_f1^2
        losvd=exp(dcomplex(shift((-0.5d*w_f2),n2-1),shift((-pars[0]*w_f),n2-1)))
        if(npars gt 2) then begin
            poly_f = 1d + dcomplex(shift(pars[3]/sqrt(24d)*(w_f2*(4d*w_f2-12d)+3d),n2-1l),$
                                   shift(pars[2]/sqrt(3d)*(w_f1*(2d*w_f2-3d)),n2-1l))
            if(npars gt 4) then $
                poly_f -= dcomplex(shift(pars[5]/sqrt(720d)*(w_f2*(w_f2*(8d*w_f2-60d)+90d)-15d),n2-1l),$
                                   shift(pars[4]/sqrt(60d)*(w_f1*(w_f2*(4d*w_f2-20d)+15d)),n2-1l))
            losvd *= poly_f
        endif
    endif else begin
        n=2*n2*factor+1
        x = n2 - 2d*n2*dindgen(n)/(n-1d) ; Evaluate the Gaussian using steps of 1/factor pixel (reverse element order)
        w = (x - pars[0])/pars[1]
        w2 = w^2
        losvd = exp(-0.5d*w2)/(sqrt(2d*!dpi)*pars[1]) ; Normalized total(Gaussian)=1

        ; Hermite polynomials normalized as in Appendix A of van der Marel & Franx (1993).
        ; These coefficients are given e.g. in Appendix C of Cappellari et al. (2002)
        if(npars gt 2) then begin
            polyn = 1d + pars[2]/Sqrt(3d)*(w*(2d*w2-3d)) $       ; H3
                      + pars[3]/Sqrt(24d)*(w2*(4d*w2-12d)+3d)   ; H4
            if(npars eq 6) then $
                polyn = polyn + pars[4]/Sqrt(60d)*(w*(w2*(4d*w2-20d)+15d)) $      ; H5
                            + pars[5]/Sqrt(720d)*(w2*(w2*(8d*w2-60d)+90d)-15d)  ; H6
            losvd = losvd*polyn
        endif
    
        if(keyword_set(noneg)) then begin
            xneg=where(x lt pars[0], cxneg, comp=xpos)
            smneg = max(where(losvd[xneg]/max(losvd) le 0.1)) ;; 5% cut-off
            smpos = min(where(losvd[xpos]/max(losvd) le 0.1))
            dneg=(losvd[xneg[smneg]]-losvd[xneg[smneg-1]])/(x[xneg[smneg]]-x[xneg[smneg-1]])
            dpos=(losvd[xpos[smpos]]-losvd[xpos[smpos-1]])/(x[xpos[smpos]]-x[xpos[smpos-1]])
            b_p = -losvd[xpos[smpos]]/dpos
            a_p = losvd[xpos[smpos]]*exp(-dpos*x[xpos[smpos]]/losvd[xpos[smpos]])
            b_n = losvd[xneg[smneg]]/dneg
            a_n = losvd[xneg[smneg]]*exp(-dneg*x[xneg[smneg]]/losvd[xneg[smneg]])
            losvd[xpos[smpos:*]]=a_p*(exp(-x[xpos[smpos:*]]/b_p))
            losvd[xneg[0:smneg]]=a_n*(exp(x[xneg[0:smneg]]/b_n))
        endif
    
        if(not keyword_set(nonorm)) then losvd=losvd/total(losvd)
    endelse

    return,losvd
end
