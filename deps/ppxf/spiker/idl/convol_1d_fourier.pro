;;;;;
; 1D convolution using a Fourier image of the kernel
; Treats NaNs/Infs as missing data
;
; INPUT:
;    data -- a 1D array with the data
;    krnl_fft -- a 1D array containing the Fourier image of the convolution kernel, must be double complex
; OPTIONAL INPUT:
;    invalid=invalid -- a parameter that specifies invalid data values to be masked in a similar fashion to NaN/Inf
; OUTPUT:
;    convolved 1D array
;;;;;

function convol_1d_fourier,data,krnl_fft,invalid=invalid
    s_data=size(data)
    s_krnl=size(krnl_fft)
    if(s_data[0] ne 1 or s_krnl[0] ne 1) then message,'Only 1-dimensional input is supported'
    n_ft=s_krnl[1]
    
    g_data=(n_elements(invalid) eq 1)? $
        where((finite(data) eq 1) and (data ne invalid), cg_data) : $
        where((finite(data) eq 1), cg_data)

    if(cg_data gt n_ft or cg_data lt 3) then message,'Incompatible dimensions of data and kernel FFT'

    data_good=dblarr(n_ft)
    data_good[0:cg_data-1]=data[g_data]
    if(cg_data lt n_ft) then $
        data_good[cg_data:*]=data_good[cg_data-1]+dindgen(n_ft-cg_data)*(data_good[0]-data_good[cg_data-1])/(n_ft-cg_data)
    
    data_good_conv=real_part(fft(fft(data_good,-1)*krnl_fft,1))
    data_conv=data
    data_conv[g_data]=data_good_conv[0:cg_data-1]

    return, data_conv
end
