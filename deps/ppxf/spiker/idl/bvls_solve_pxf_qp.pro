; Replacement for the Cappellari's bvls_solve_pxf.
; Modifications: la_least_square instead of svsol and fast quadprog based
; bounded least squares algorithm

function bvls_solve_pxf_qp, A, b, npoly, method=method
compile_opt idl2, hidden

; No need to enforce positivity constraints if fitting one
; single template: use faster SVD solution instead of BVLS.
;
s = size(a)
if s[0] eq 1 then begin ; A is a vector, not an array
    sol = total(A*b,/DOUBLE)/total(A^2,/DOUBLE)
endif else if s[2] eq npoly+1 then begin ; Fitting a single template
    sol = bounded_least_squares(transpose(a),b,/double,method=method)
endif else begin               ; Fitting multiple templates
    bnd = dblarr(2,s[2],/NOZERO)
    bnd[0,0:npoly] = -!values.d_infinity ; No bounds on Legendre polynomials
    bnd[1,*] = !values.d_infinity
    bnd[0,npoly:*]=0d ; Positivity constraints on the templates
    mx = double((machar(/DOUBLE)).xmax)
    sol = bounded_least_squares(transpose(a),b,bnd,/double,status=status,method=method)
    if status ne 0 then message, 'BVLS Error n. ' + strtrim(status,2)
endelse

return, sol
END
