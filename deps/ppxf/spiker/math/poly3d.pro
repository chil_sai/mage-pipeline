function poly3d,x,y,z,c0,deg1=deg1,deg2=deg2,deg3=deg3,irregular=irregular

c=c0
s=size(c)
if(s[0] eq 3) then begin
    deg1a=s[1]-1L
    deg2a=s[2]-1L
    deg3a=s[3]-1L
    c1=c ;;;;transpose(c)
endif else message,'Only 3D coefficient arrays are supported for a moment'

nx=n_elements(x)
ny=n_elements(y)
nz=n_elements(z)
if(keyword_set(irregular) and ((nx ne ny) or (nx ne nz))) then message,'X and Y should have equal number of elements when IRREGULAR is set'

if(keyword_set(irregular)) then begin
    x_crd=x
    y_crd=y
    z_crd=z
endif else begin
    x_crd=reform(transpose(congrid(transpose([x]),ny,nx,nz)),nx*ny*nz)
    y_crd=reform(congrid(transpose(y),nx,ny,nz),nx*ny*nz)
    z_crd=reform(congrid(transpose(z),nx,ny,nz),nx*ny*nz)
endelse

res = (keyword_set(irregular))? dblarr(nx) : dblarr(nx*ny*nz)
;print,size(res),nx,ny
;print,c1
;print,'deg1a,deg2a=',deg1a,deg2a
;print,'size(c1)=',size(c1)
;print,float(c1)
for i=0, deg1a do begin
    for j=0, deg2a do begin
        for k=0, deg3a do begin
            res = res+x_crd^i*y_crd^j*z_crd^k*c1[i,j,k]
        endfor
    endfor
endfor

if(not keyword_set(irregular)) then res=reform(res,nx,ny,nz)
return,res
end
