; bounded_least_squares, a wrapper for the two bvls routines re-coded by
; Philippe Prugniel for the ULySS package from original Fortran routines,
; which acts as a function with a calling sequence similar to that of
; LA_LEAST_SQUARES with some additional arguments and can be used as a
; drop-in replacement for LA_LEAST_SQUARES if called without bounds
;
; Written by Igor Chilingarian on 2020/Apr/17

function bounded_least_squares, A, b, bnd_inp, status=status, quiet=quiet,$
    method=method, nnls=nnls, force_lapack=force_lapack, $
    rank=rank, rcondition=rcondition, residual=residual, $ ; la_least_squares arguments
    rnorm=rnorm, itmax=itmax, eps=eps, $                   ; common arguments among bvls_ps and bvls_lh
    istate=istate, loop=loop, double=double, $           ; bvls_ps arguments
    nsetp=nsetp, w=w, index=index                          ; bvls_lh arguments

    if(n_elements(method) ne 1) then method=0              ; 0=quadprog_solve; 1=Lawson & Hanson;  2=Parker & Stark

    s_a = size(A)
    npar = s_a[1] ; number of parameters to fit    
    s_b = size(b)
    n_k = (s_b[0] eq 1)? 1 : s_b[1] ; number of linear systems to solve
    
    if(n_params() eq 2 and ~keyword_set(nnls)) then $
        if(keyword_set(force_lapack)) then $      ; default to la_least_squres if no bounds/nnls is given
            return, la_least_squares(A, b, double=double, method=method, rank=rank,$
                status=status, rcondition=rcondition, residual=residual) $
        else bnd_inp=(method eq 1)? [-!values.d_infinity,!values.d_infinity] : transpose([[dblarr(npar)-!values.d_infinity],[dblarr(npar)+!values.d_infinity]])

    if(n_k gt 1) then xarr=dblarr(n_k,npar)

    ; the /nnls keyword sets bounds to solve a non-neative least squares problem 
    bnd = (keyword_set(nnls))? transpose([[dblarr(npar)],[dblarr(npar)+!values.d_infinity]]) : bnd_inp

    if(method eq 0) then begin
        dmat = matrix_multiply(a,a,/btrans)
        bndmin = where(bnd[0,*] gt -(machar()).xmax, cbndmin, compl=unbndmin, ncompl=cunbndmin)
        bndmax = where(bnd[1,*] lt  (machar()).xmax, cbndmax, compl=unbndmax, ncompl=cunbndmax)

        if(cbndmin+cbndmax gt 0) then begin
            a2mat = dblarr(npar, cbndmin+cbndmax)
            b2vec = dblarr(cbndmin+cbndmax)
            if(cbndmin gt 0) then begin
                a2mat[*,0:cbndmin-1]=(identity(npar,/double))[*,bndmin]
                b2vec[0:cbndmin-1]=bnd[0,bndmin]
            endif
            if(cbndmax gt 0) then begin
                a2mat[*,cbndmin:*]=(-identity(npar,/double))[*,bndmax]
                b2vec[cbndmin:*]=-bnd[1,bndmax]
            endif
        endif

        for k=0L,n_k-1L do begin
            dvec = (s_b[0] eq 1) ? transpose(matrix_multiply(b,a,/atrans,/btrans)) : transpose(matrix_multiply(b[k,*],a,/btrans))
            x = quadprog_solve(dmat, dvec, a2mat=a2mat, b2vec=b2vec, status=status, cfirst=0, crval=crval); ,/no_check_constraints) ;, /debug)
            if(status eq 0) then begin
                ; sometime a value can go barely above the limit by some 1e-16
                ; because of numerical errors. correcting it here
                if(cbndmin gt 0) then x[bndmin] = (bnd[0,bndmin] > x[bndmin])
                if(cbndmax gt 0) then x[bndmax] = (bnd[1,bndmax] < x[bndmax])
                if(arg_present(residual)) then begin
                    if(k eq 0 and n_k gt 1) then residual=dblarr(n_k)
                    if(n_k eq 1) then $
                        residual=(crval + matrix_multiply(b,b,atrans=(s_b[0] eq 1),btrans=(s_b[0] ne 1))/2d)^2 $
                    else $
                        residual[k]=(crval + matrix_multiply(b[k,*],b[k,*],/btrans)/2d)^2
                endif
            endif else begin ;; default to bvls_lh if quadprog failed for whatever reason (usual reason is a degenerate system of equations)
                if(n_elements(A_inp) ne n_elements(a)) then begin
                    A_inp = (keyword_set(double))? double(transpose(a)) : transpose(a)
                    bnd_inp = bnd
                endif
                b_inp = (keyword_set(double))? double((s_b[0] eq 1 ? b : transpose(b[k,*]))) : (s_b[0] eq 1 ? b : transpose(b[k,*]))
                bvls_lh, A_inp, b_inp, bnd_inp, x, rnorm=rnorm, itmax=itmax, eps=eps, nsetp=nsetp, w=w, index=index, status=status, quiet=quiet
            endelse
            if(n_k gt 1) then xarr[k,*] = transpose(x)
        endfor
    endif else if(method eq 1) then begin
        A_inp = (keyword_set(double))? double(transpose(a)) : transpose(a)
        bnd_inp = bnd
        for k=0L,n_k-1L do begin
            b_inp = (keyword_set(double))? double((s_b[0] eq 1 ? b : transpose(b[k,*]))) : (s_b[0] eq 1 ? b : transpose(b[k,*]))
;            bvls, A_inp, b_inp, bnd_inp, x, rnorm=rnorm, itmax=itmax, eps=eps, w=w, index=index
            bvls_lh, A_inp, b_inp, bnd_inp, x, rnorm=rnorm, itmax=itmax, eps=eps, nsetp=nsetp, w=w, index=index, status=status, quiet=quiet
            if(n_k gt 1) then xarr[k,*] = transpose(x)
        endfor
    endif else if(method eq 2) then begin
        A_inp = (keyword_set(double))? double(transpose(A)) : transpose(A)
        for k=0L,n_k-1L do begin
            if(n_elements(istate) eq npar) then $
                bvls_ps, A_inp, (s_b[0] eq 1 ? b : transpose(b[k,*])), bnd, x, istate, rnorm=rnorm, itmax=itmax, eps=eps, loop=loop, double=double, status=status, quiet=quiet $
            else $
                bvls_ps, A_inp, (s_b[0] eq 1 ? b : transpose(b[k,*])), bnd, x, rnorm=rnorm, itmax=itmax, eps=eps, loop=loop, double=double, status=status, quiet=quiet
            if(n_k gt 1) then xarr[k,*] = transpose(x)
        endfor
    endif else message,'Method='+strcompress(string(method))+' is not supported'

    return, (n_k gt 1? xarr : (s_b[1] eq 1 ? transpose(x) : x))
end
