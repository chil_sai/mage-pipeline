;  Copyright (C) 1995, 1996, 1997, 1998
;  Berwin A. Turlach <bturlach@stats.adelaide.edu.au>
;  $Id: solve.QP.f,v 1.15 1998/07/23 05:23:26 bturlach Exp $
; 
;  This library is free software; you can redistribute it and/or
;  modify it under the terms of the GNU Library General Public
;  License as published by the Free Software Foundation; either
;  version 2 of the License, or (at your option) any later version.
;  
;  This library is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;  Library General Public License for more details.
;  
;  You should have received a copy of the GNU Library General Public
;  License along with this library; if not, write to the Free Software
;  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;  MA 02111-1307 USA
; 
;  This routine uses the Goldfarb/Idnani algorithm to solve the
;  following minimization problem:
;
;        minimize  -d^T x + 1/2 *  x^T D x
;        where   A1^T x  = b1
;                A2^T x >= b2
;
;  the matrix D is assumed to be positive definite.  Especially,
;  w.l.o.g. D is assumed to be symmetric.
;  
;  Input parameter:
;  dmat   nxn matrix, the matrix D from above (dp)
;         *** WILL BE DESTROYED ON EXIT ***
;         The user has two possibilities:
;         a) Give D (ierr=0), in this case we use routines from LINPACK
;            to decompose D.
;         b) To get the algorithm started we need R^-1, where D=R^TR.
;            So if it is cheaper to calculate R^-1 in another way (D may
;            be a band matrix) then with the general routine, the user
;            may pass R^{-1}.  Indicated by ierr not equal to zero.
;  dvec   nx1 vector, the vector d from above (dp)
;         *** WILL BE DESTROYED ON EXIT ***
;         contains on exit the solution to the initial, i.e.,
;         unconstrained problem
;  fddmat scalar, the leading dimension of the matrix dmat
;  n      the dimension of dmat and dvec (int)
;  amat   nxq matrix, the matrix A from above (dp) [ A=(A1 A2) ]
;         *** ENTRIES CORRESPONDING TO EQUALITY CONSTRAINTS MAY HAVE
;             CHANGED SIGNES ON EXIT ***
;  bvec   qx1 vector, the vector of constants b in the constraints (dp)
;         [ b = (b1^T b2^T)^T ]
;         *** ENTRIES CORRESPONDING TO EQUALITY CONSTRAINTS MAY HAVE
;             CHANGED SIGNES ON EXIT ***
;  fdamat the first dimension of amat as declared in the calling program. 
;         fdamat >= n !!
;  q      integer, the number of constraints.
;  meq    integer, the number of equality constraints, 0 <= meq <= q.
;  ierr   integer, code for the status of the matrix D:
;            ierr =  0, we have to decompose D
;            ierr != 0, D is already decomposed into D=R^TR and we were
;                       given R^{-1}.
;
;  Output parameter:
;  sol   nx1 the final solution (x in the notation above)
;  crval scalar, the value of the criterion at the minimum      
;  iact  qx1 vector, the constraints which are active in the final
;        fit (int)
;  nact  scalar, the number of constraints active in the final fit (int)
;  iter  2x1 vector, first component gives the number of "main" 
;        iterations, the second one says how many constraints were
;        deleted after they became active
;  ierr  integer, error code on exit, if
;           ierr = 0, no problems
;           ierr = 1, the minimization problem has no solution
;           ierr = 2, problems with decomposing D, in this case sol
;                     contains garbage!!
;
;  Working space:
;  work  vector with length at least 2*n+r*(r+5)/2 + 2*q +1
;        where r=min(n,q)
;
;
;  Converted from Fortran to IDL by Igor Chilingarian, CfA on 2020-Apr-13
;  Changes with respect to the original Fortran code:
;     -- converted into a function that returns the optimal solution
;     -- separate equality (a1mat, b1vec) and inequality (a2mat, b2vec)
;        constraints supplied as input keywords
;     -- the unconstrained solution is returned via the keyword sol_uc=
;        will be the same as the return value if no constraints were violated
;     -- added the /cfirst keyword to choose whether to use the first
;        encountered violated constrained insted of the worst one
;     -- removed all GOTOs: needs to be exhaustively tested (2020-Apr-13)
;     -- added the /debug keyword to provide a lot of debug output
;     -- added the /no_check_constraints not to remove non-unique constraints
;        by defaults they will be removed

function quadprog_solve,dmat_inp, dvec_inp, a1mat=a1mat, b1vec=b1vec, $
                        dmat_rinv=dmat_rinv,warmstart=warmstart,$
                        a2mat=a2mat, b2vec=b2vec, no_check_constraints=no_check_constraints,$
                        crval=crval, iact=iact, nact=nact, cfirst=cfirst,$
                        n_iter=n_iter, status=status, sol_uc=sol_uc, debug=debug, work=work

;;;;;; The original call sequence and declarations of variables
;      subroutine qpgen2(dmat, dvec, fddmat, n, sol, crval, amat, 
;     *     bvec, fdamat, q, meq, iact, nact, n_iter, work, status)  
;      integer n, i, j, l, l1, fdamat, fddmat,
;     *     info, q, iact(*), n_iter(*), it1,
;     *     status, nact, iwzv, iwrv, iwrm, iwsv, iwuv, nvl,
;     *     r, iwnbv, meq
;      double precision dmat(fddmat,*), dvec(*),sol(*), bvec(*),
;     *     work(*), temp, sum, t1, tt, gc, gs, crval,
;     *     nu, amat(fdamat,*)
;      logical t1inf, t2min
;;;;;;

    t1inf = 0
    t2min = 0

    status=0

    dmat=dmat_inp
    dvec=dvec_inp
    s_dmat=size(dmat)
    n=n_elements(dvec)
    if(s_dmat[0] ne 2 or (s_dmat[1] ne s_dmat[2]) or n ne s_dmat[1]) then message,'Incorrect input! Aborting'

    q = n_elements(b1vec) + n_elements(b2vec)
    meq = n_elements(b1vec)
    if(keyword_set(debug)) then print,'number of constraints:',q,' of them equality constraints:',meq
    if(q gt 0) then begin
        if(~keyword_set(no_check_constraints)) then begin
            if(meq gt 0) then begin
                norm_a1mat=transpose([sqrt(total(a1mat^2,1))])
                tmp_eqc=[a1mat,transpose(b1vec)]/rebin(norm_a1mat,n_elements(a1mat[*,0])+1L,n_elements(b1vec))
                uq_eqc=uniq_vectors(tmp_eqc,multisort_array(transpose(tmp_eqc)))
                a1mat_inp=a1mat[*,uq_eqc]
                b1vec_inp=b1vec[uq_eqc]
                q-=(meq-n_elements(uq_eqc))
                meq=n_elements(uq_eqc)
            endif
            if(meq lt q) then begin
                norm_a2mat=transpose([sqrt(total(a2mat^2,1))])
                tmp_nec=[a2mat,transpose(b2vec)]/rebin(norm_a2mat,n_elements(a2mat[*,0])+1L,n_elements(b2vec))
                uq_nec=uniq_vectors(tmp_nec,multisort_array(transpose(tmp_nec)))
                a2mat_inp=a2mat[*,uq_nec]
                b2vec_inp=b2vec[uq_nec]
                q-=(n_elements(b2vec)-n_elements(uq_nec))
            endif            
            if(keyword_set(debug)) then print,'number of unique constraints:',q,' of them equality constraints:',meq
        endif else begin
            if(meq gt 0) then begin
                a1mat_inp=a1mat
                b1vec_inp=b1vec
            endif
            if(meq lt q) then begin
                a2mat_inp=a2mat
                b2vec_inp=b2vec
            endif
        endelse

        bvec=dblarr(q)
        amat=dblarr(n,q)
        if(meq gt 0) then begin
            bvec[0:meq-1]=b1vec_inp
            if(n_elements(a1mat_inp) ne n*meq) then message,'a1mat has incorrect dimensions'
            amat[*,0:meq-1]=a1mat_inp[*,0:meq-1]
        endif
        if(meq lt q) then begin
            bvec[meq:*]=b2vec_inp
            if(n_elements(a2mat_inp) ne n*(q-meq)) then message,'a2mat has incorrect dimensions'
            amat[*,meq:*]=a2mat_inp
        endif
        iact=lonarr(q)
    endif else iact=-1

    n_iter=lonarr(2)

    r = (n<q)

; 
; store the initial dvec to calculate below the unconstrained minima of
; the critical value.
;
    l = 2L*n + (r*(r+5L))/2 + 2L*q + 1L
    work=dblarr(l)
    work[0:n-1]=dvec

;
; get the initial solution
;
    if(~keyword_set(warmstart) or n_elements(dmat_inv) ne n^2) then begin
        dmat_chol=dmat
        la_choldc, dmat_chol, /double, /upper, status=info
        if(info ne 0) then begin
            status = 2
            message,/inf,'Problem with Cholesky factorization of D'
            return, !values.d_nan
        endif
        for i=1L,n-1L do dmat_chol[0:i-1L,i]=0d
        ; sol_uc = la_cholsol(dmat_chol, dvec, /double, /upper)
        dmat_inv = la_invert(dmat_chol, /double, status=info)
        if(info ne 0) then begin
            status = 2
            message,/inf,'Problem while inverting D'
            return, !values.d_nan
        endif
    endif
;        
; Matrix D is already factorized, so we have to multiply d first with 
; R^-T and then with R^-1.  R^-1 is stored in the upper half of the
; array dmat.
;
    sol_uc = matrix_multiply(dmat_inv,matrix_multiply(dmat_inv,dvec),/atr)
    if(keyword_set(debug)) then print,'unconstrained solution: ',sol_uc
    sol = sol_uc

    dmat = dmat_inv
    dvec = sol_uc
;
; set lower triangular of dmat to zero, store dvec in sol and
; calculate value of the criterion at unconstrained minima
;
; the lower triangular of dmat is already zero
    sol = dvec
    crval = -total(work[0:n-1]*sol)/2d
    work[0:n-1]=0d
    status = 0
    if(q eq 0) then return, sol
;
; calculate some constants, i.e., from which index on the different
; quantities are stored in the work matrix
;
    iwzv  = n
    iwrv  = iwzv + n
    iwuv  = iwrv + r
    iwrm  = iwuv + r+1
    iwsv  = iwrm + (r*(r+1))/2
    iwnbv = iwsv + q
;
; calculate the norm of each column of the A matrix
;
    work[iwnbv:iwnbv+q-1] = sqrt(total(amat^2,1))
    if(keyword_set(debug)) then print,'norm_Amat=',work[iwnbv:iwnbv-1+q]
    nact = 0L
    n_iter[0] = 0L
    n_iter[1] = 0L

    start_it = 1 ; a temporary variable to tell whether to start the loop from the beginning (1) or skip the first part (0)
    while(1) do begin
L50:    if(keyword_set(debug)) then print,(start_it ? 'label 50' : 'label 55')
        if(start_it) then begin
;
; start a new iteration      
;
            n_iter[0]++
;
; calculate all constraints and check which are still violated
; for the equality constraints we have to check whether the normal
; vector has to be negated (as well as bvec in that case)
;
            l = iwsv
            sum_vec = -bvec + transpose(amat##transpose(sol))
            if(meq gt 0) then begin
                work[l:l+meq-1]=-abs(sum_vec[0:meq-1])
                sum_pos_idx = where(sum_vec[0:meq-1] gt 0, csum_pos_idx)
                if(csum_pos_idx gt 0) then begin
                    amat[*,sum_pos_idx]*=-1d
                    bvec[sum_pos_idx]*=-1d
                endif
            endif
            if(meq lt q) then work[l+meq:l+q-1]=sum_vec[meq:*]
            l += q
;
; as safeguard against rounding errors set already active constraints
; explicitly to zero
;
            if(nact gt 0) then work[iwsv+iact[0:nact-1]-1] = 0d
; 
; we weight each violation by the number of non-zero elements in the
; corresponding row of A. then we choose the violated constraint which
; has maximal absolute value, i.e., the minimum.
; by obvious commenting and uncommenting we can choose the strategy to      
; take always the first constraint which is violated. ;-)
;
            if(keyword_set(debug)) then print,'nact=',[nact],' constraints: ',work[iwsv:iwsv+q-1]/work[iwnbv:iwnbv+q-1],' sumvec:',-bvec+transpose(sol#amat),' sol:',sol,' iact:',iact
            nvl = 0L 
            temp = 0d
            negval = where(work[iwsv:iwsv+q-1]/work[iwnbv:iwnbv+q-1] lt 0d, cnegval)
            if(cnegval gt 0) then if(keyword_set(cfirst)) then nvl=negval[0]+1L else begin
                minneg=min(work[iwsv+negval]/work[iwnbv+negval],midx)
                nvl=negval[midx]+1L
            endelse
            if(keyword_set(debug)) then print,'NVL=',nvl

            if(nvl eq 0) then return, sol
        endif
;     
; calculate d=J^Tn^+ where n^+ is the normal vector of the violated
; constraint. J is stored in dmat in this implementation!!
; if we drop a constraint, we have to jump back here.
;
L55:    if(keyword_set(debug)) then print,'label 55'
        work[0:n-1]=dmat # amat[*,nvl-1]
;
; Now calculate z = J_2 d_2
;
        l1 = iwzv
        if(nact lt n) then work[l1:l1+n-1] = transpose(dmat[nact:n-1,*] ## transpose(work[nact:n-1]))
;
; and r = R^{-1} d_1, check also if r has positive elements (among the 
; entries corresponding to inequalities constraints).
;
        t1inf = 1
        for i=nact,1L,-1L do begin
            sum = work[i-1]
            l  = iwrm+(i*(i +3L))/2L
            l1 = l-i
            if(nact gt i) then sum-= total(work[l+(lindgen(nact-i))*(2L*i+lindgen(nact-i)+1L)/2-1L]*work[iwrv+i:iwrv+nact-1L])
            sum = sum / work[l1-1L]
            work[iwrv+i-1] = sum
            if ~((iact[i-1] le meq) or (sum le 0d)) then begin
                t1inf = 0
                it1 = i
            endif
        endfor
;
; if r has positive elements, find the partial step length t1, which is
; the maximum step in dual space without violating dual feasibility.
; it1  stores in which component t1, the min of u/r, occurs.
; 
        if(~t1inf) then begin
            t1   = work[iwuv+it1-1]/work[iwrv+it1-1]
            for i=1L,nact do begin
                if ~((iact[i-1] le meq) or (work[iwrv+i-1] le 0d)) then begin
                    temp = work[iwuv+i-1]/work[iwrv+i-1]
                    if(temp lt t1) then begin
                        t1   = temp
                        it1  = i
                    endif
                endif
            endfor
        endif 
;
; test if the z vector is equal to zero
;
        sum = total(work[iwzv:iwzv+n-1]^2)
        sum_check = sum
        if(sum_check eq 0d) then begin
;         
; No step in primal space such that the new constraint becomes
; feasible. Take step in dual space and drop a constant.
;     
            if(t1inf) then begin
;            
; No step in dual space possible either, problem is not solvable
;
                status = 1
                message,/inf,'Problem has no solution, returning a vector of NaNs'
                return, !values.d_nan+sol
            endif else begin
;
; we take a partial step in dual space and drop constraint it1,
; that is, we drop the it1-th active constraint.
; then we continue at step 2(a) (marked by label 55)
;
                if(nact gt 0) then work[iwuv:iwuv+nact-1] = work[iwuv:iwuv+nact-1] - t1*work[iwrv:iwrv+nact-1]
                work[iwuv+nact] = work[iwuv+nact] + t1
                sum_check = sum ; goto, L700 ;; (sum eq 0d and ~t1inf)
            endelse
        endif else begin
;
; compute full step length t2, minimum step in primal space such that
; the constraint becomes feasible.
; keep sum (which is z^Tn^+) to update crval below!
;        
            sum = total(work[iwzv:iwzv+n-1]*amat[*,nvl-1])
            tt = -work[iwsv+nvl-1L]/sum
            t2min = 1
            if(~t1inf) then begin
                if(t1 lt tt) then begin
                    tt = t1
                    t2min = 0
                endif
            endif
;
; take step in primal and dual space
;
            if(keyword_set(debug)) then print,'crval,tt,sum,iwuv,nvl,nact',crval,tt,sum,iwuv,nvl,[nact],' sol_noa:',sol
            sol = sol + tt*work[iwzv:iwzv+n-1]
            if(keyword_set(debug)) then print,[0],' sol_adj:',sol
            crval = crval + tt*sum*(tt/2d + work[iwuv+nact])
            if(nact gt 0) then work[iwuv:iwuv+nact-1] = work[iwuv:iwuv+nact-1] - tt*work[iwrv:iwrv+nact-1]
            work[iwuv+nact] = work[iwuv+nact] + tt
;
; if it was a full step, then we check wheter further constraints are
; violated otherwise we can drop the current constraint and iterate once
; more 
            if(t2min) then begin
;
; we took a full step. Thus add constraint nvl to the list of active
; constraints and update J and R
;
                nact++
                iact[nact-1] = nvl
;
; to update R we have to put the first nact-1 components of the d vector
; into column (nact) of R
;
                l = iwrm + ((nact-1L)*nact)/2L + 1L
                if(nact gt 1) then work[l-1:l+nact-3]=work[0:nact-2]
                l+=(nact-1L)
;
; if now nact=n, then we just have to add the last element to the new
; row of R.
; Otherwise we use Givens transformations to turn the vector d(nact:n)
; into a multiple of the first unit vector. That multiple goes into the
; last element of the new row of R and J is accordingly updated by the
; Givens transformations. 
;
                if(nact eq n) then begin
                    work[l-1] = work[n-1]
                endif else begin
                    for i=n,nact+1L,-1L do begin
;
; we have to find the Givens rotation which will reduce the element
; (l1) of d to zero.
; if it is already zero we don't have to do anything, except of
; decreasing l1
;
                        if(work[i-1L] eq 0d) then continue
                        gc   = (abs(work[i-2L]) > abs(work[i-1L]))
                        gs   = (abs(work[i-2L]) < abs(work[i-1L]))
                        temp = abs(gc)*sqrt(1d +gs*gs/(gc*gc))*((work[i-2L] ge 0)? 1d : -1d)
                        gc   = work[i-2L]/temp
                        gs   = work[i-1L]/temp
; 
; The Givens rotation is done with the matrix (gc gs, gs -gc).
; If gc is one, then element (i) of d is zero compared with element
; (l1-1). Hence we don't have to do anything. 
; If gc is zero, then we just have to switch column (i) and column (i-1) 
; of J. Since we only switch columns in J, we have to be careful how we
; update d depending on the sign of gs.
; Otherwise we have to apply the Givens rotation to these columns.
; The i-1 element of d has to be updated to temp.                  
;
                        if(gc eq 1d) then continue
                        if(gc eq 0d) then begin
                            work[i-2L] = gs * temp
                            dmat[i-2L:i-1L,*]=dmat[[i-1L,i-2L],*]
                        endif else begin
                            work[i-2L] = temp
                            nu = gs/(1d +gc)

                            temp = gc*dmat[i-2L,*] + gs*dmat[i-1L,*]
                            dmat[i-1L,*] = nu*(dmat[i-2L,*]+temp) - dmat[i-1L,*]
                            dmat[i-2L,*] = temp
                        endelse
                    endfor
;     
; l is still pointing to element (nact,nact) of the matrix R.
; So store d(nact) in R(nact,nact)
                    work[l-1] = work[nact-1]
                endelse
            endif else begin
;
; we took a partial step in dual space. Thus drop constraint it1,
; that is, we drop the it1-th active constraint.
; then we continue at step 2(a) (marked by label 55)
; but since the fit changed, we have to recalculate now "how much"
; the fit violates the chosen constraint now.
;
                sum = -bvec[nvl-1L] + total(sol*amat[*,nvl-1L])
                if(nvl gt meq) then begin
                    work[iwsv+nvl-1L] = sum
                endif else begin
                    work[iwsv+nvl-1L] = -abs(sum)
                    if(sum gt 0d) then begin
                        amat[*,nvl-1L] = -amat[*,nvl-1L]
                        bvec[i-1L] = -bvec[i-1L]
                    endif
                endelse
                if(keyword_set(debug)) then print,'Constraint: ',nvl-1,' value=',sum
                sum_check = sum ; goto, L700 ;;; (sum ne 0d and ~t2min)
            endelse
        endelse
        ;start_it = 1
        ;goto,L50
        if(keyword_set(debug)) then print,'L50 or L55? sum, t1inf, t2min=',sum_check, t1inf, t2min

        if((sum_check eq 0d and ~t1inf) or (sum_check ne 0d and ~t2min)) then begin
L700:       if(keyword_set(debug)) then print,'label 700'
;
; Drop constraint it1
;
;
; if it1 = nact it is only necessary to update the vector u and nact
;
            if(it1 ne nact) then begin   ;  if(it1 eq nact) then goto, L799
;
; After updating one row of R (column of J) we will also come back here
;
                while (it1 lt nact) do begin
L797:               if(keyword_set(debug)) then print,'label 797',' it1=',it1
;
; we have to find the Givens rotation which will reduce the element
; (it1+1,it1+1) of R to zero.
; if it is already zero we don't have to do anything except of updating
; u, iact, and shifting column (it1+1) of R to column (it1)
; l  will point to element (1,it1+1) of R
; l1 will point to element (it1+1,it1+1) of R
;
                    l  = iwrm + (it1*(it1+1L))/2L + 1L
                    l1 = l+it1
                    if(work[l1-1] ne 0d) then begin ;    if(work[l1-1] eq 0d) then goto, L798
                        gc   = abs(work[l1-2L])>abs(work[l1-1L])
                        gs   = abs(work[l1-2L])<abs(work[l1-1L])
                        temp = abs(gc)*sqrt(1+gs*gs/(gc*gc))*((work[l1-2L] ge 0)? 1d : -1d)
                        gc   = work[l1-2L]/temp
                        gs   = work[l1-1L]/temp
; 
; The Givens rotation is done with the matrix (gc gs, gs -gc).
; If gc is one, then element (it1+1,it1+1) of R is zero compared with
; element (it1,it1+1). Hence we don't have to do anything.
; if gc is zero, then we just have to switch row (it1) and row (it1+1)
; of R and column (it1) and column (it1+1) of J. Since we switch rows in
; R and columns in J, we can ignore the sign of gs.
; Otherwise we have to apply the Givens rotation to these rows/columns.
;
                        if(gc ne 1d) then begin    ; if(gc eq 1d) then goto, L798
                            if(gc eq 0d) then begin  
                                for i=it1+1L,nact do begin
                                    work[l1-2L:l1-1L]=work[[l1-1L,l1-2L]]
                                    l1 = l1+i
                                endfor
                                dmat[it1-1L:it1,*]=dmat[[it1,it1-1L],*]
                            endif else begin
                                nu = gs/(1d +gc)
                                for i=it1+1L,nact do begin
                                    temp       = gc*work[l1-2L] + gs*work[l1-1L]
                                    work[l1-1L] = nu*(work[l1-2L]+temp) - work[l1-1L]
                                    work[l1-2L] = temp
                                    l1 = l1+i
                                endfor

                                temp = gc*dmat[it1-1L,*] + gs*dmat[it1,*]
                                dmat[it1,*] = nu*(dmat[it1-1L,*]+temp) - dmat[it1,*]
                                dmat[it1-1L,*] = temp
                            endelse
                        endif
                    endif
;
; shift column (it1+1) of R to column (it1) (that is, the first it1
; elements). The posit1on of element (1,it1+1) of R was calculated above
; and stored in l.
;
L798:               if(keyword_set(debug)) then print,'label 798',' it1=',it1
                    l1 = l - it1
                    work[l1-1:l1+it1-2]=work[l-1:l+it1-2]
                    l+= (it1 - 1)
                    l1+= (it1 - 1)
;      
; update vector u and iact as necessary
; Continue with updating the matrices J and R
;
                    work[iwuv+it1-1] = work[iwuv+it1]
                    iact[it1-1]      = iact[it1]
                    it1++
                endwhile   ;            if(it1 lt nact) then goto, L797
            endif
L799:       if(keyword_set(debug)) then print,'label 799'

            work[iwuv+nact-1] = work[iwuv+nact]
            work[iwuv+nact] = 0d
            iact[nact-1] = 0L
            nact--
            n_iter[1]++

            start_it = 0
        endif else start_it = 1
    endwhile

    return, !values.d_nan ; we should never reach this point
end
