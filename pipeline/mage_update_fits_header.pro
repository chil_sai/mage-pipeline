; updating FITS header keywords in MagE FITS files
; applying precession to RA/DEC, computing MJD, barycentric velocity

pro mage_update_fits_header,h_obj

    ra_cur=sxpar(h_obj,'RA-D')
    dec_cur=sxpar(h_obj,'DEC-D')
    equinox_cur=sxpar(h_obj,'EQUINOX')
    sxaddpar,h_obj,'O_RA',ra_cur,' Right ascension on the equinox of observation'
    sxaddpar,h_obj,'O_DEC',dec_cur,' Declination on the equinox of observation'
    sxaddpar,h_obj,'OEQUINOX',equinox_cur,' Equinox at the time of observation'
    sxdelpar,h_obj,'RA-D'
    sxdelpar,h_obj,'DEC-D'
    ra2000=ra_cur
    dec2000=dec_cur
    precess,ra2000,dec2000,equinox_cur,2000.0

    sxaddpar,h_obj,'RA',ra2000,' [deg] Right ascension of the target'
    sxaddpar,h_obj,'DEC',dec2000,' [deg] Declination of the target'
    sxaddpar,h_obj,'EQUINOX',2000.0,' Equinox of coordinates (RA,DEC)'
    sxaddpar,h_obj,'RADECSYS','FK5',' Coordinate system'

    date_obs=sxpar(h_obj,'UT-DATE')+'T'+sxpar(h_obj,'UT-TIME')
    sxaddpar,h_obj,'DATE-OBS',date_obs,'ISO8601 UTC date (start)'
    mjd=date_conv(date_obs,'M')
    sxaddpar,h_obj,'MJD',mjd,'UTC Modified Julian Date (start)',after='DATE-OBS'
    baryvel,mjd+2400000.5d,2000.0,vh,vb
    vel=vb[0]*cos(dec2000/!radeg)*cos(ra2000/!radeg)+vb[1]*cos(dec2000/!radeg)*sin(ra2000/!radeg)+vb[2]*sin(dec2000/!radeg)
    sxaddpar,h_obj,'BARYVEL',vel,' [km/s] barycentric correction (NOT APPLIED)'

end
