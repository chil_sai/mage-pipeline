
data_path='/Users/chil/Data/raw/Echelle/MagE/ut170705_06/'
outp_path='/Users/chil/work/MagE/test/'
outname = 'obj1_170705_001'
telname = 'HIP60327'

conf_str={ $
    flat_file:data_path+'mage'+string([82,83,84,85,86,87,88],format='(i4.4)')+'.fits.gz' ,$
    xe_file:data_path+'mage'+string([59,60,61,62,63],format='(i4.4)')+'.fits.gz',$
    arc_file:data_path+'mage'+['0067','0068','0069']+'.fits.gz',$
    obj_file:data_path+'mage0064.fits.gz',$
    cr_reject:1,$           ;;; cosmic ray hit rejection using LACosmic
    sky_mask:[1,1,1],$      ;;; sky slitmask bottom-to-top: 0=exclude; 1=include
    sky_file:data_path+'mage0065.fits.gz',$     ;;; offset sky exposure
    sky_normexp:-1.0,$       ;;; sky exposure normalization factor
    sub_sky:1,$             ;;; sky subtraction (1=Yes/0=No)
    corr_tel:1,$            ;;; perform telluric correction (1=Yes/0=No)
    flux_corr:1,$           ;;; perform flux calibration (1=Yes/0=No, corr_tel must be Yes for Yes)
    outfile_merged: outp_path+outname+'_merged.fits',$             ;;; merged flux calibrated output (no telluric correction)
    outfile_merged_corr: outp_path+outname+'_merged_corr.fits',$   ;;; merged telluric corrected flux calibrated output
    raw_2d_output:1, $
    outfile_orders: outp_path+outname+'_test_2d.fits' $            ;;; unmerged flux calibrated output (no telluric correction)
}

tel_conf_str={ $
    tel_file:data_path+'mage0070.fits.gz',$
    tel_arc_file:data_path+'mage'+['0072','0073','0074']+'.fits.gz',$ ;;; telluric specific arc frames
    tel_arc_sci:0,$         ;;; use science arc frames (1=Yes/0=No)
    tel_sci_frame:0,$       ;;; use science frame as telluric (1=Yes/0=No)
    sky_mask_tel:[0,1,1],$  ;;; telluric sky subtraction mask, same as on science frame
    sub_sky_tel:1,$         ;;; subtract sky from telluric (1=Yes/0=No)
    tel_slit_reg:2+indgen(24),$ ;;; slit goes from 0 to 36 pix, bottom-to-top
    tel_vr0: 0.0,$         ;;; radial velocity of the telluric star (initial guess)
    tel_vsini0:100.0,$      ;;; rotational velocity v*sin(i) of the telluric star (initial guess)
    tel_spec_list:file_search(getenv('MAGE_PIPELINE_PATH')+'calib_MagE/stellar_templates/phoenix/phx20atm_09*_p00_p00*'),$
    tel_novsini:0,$         ;;; skip fitting for v*sin(i) of the telluric star (0=Fitting/1=Fix)
    tel_gausslosvd:0,$      ;;; LOSVD shape for the telluric star (0=Rot/1=Gaussian); for Gaussian vsini becomes sigma
    tel_texp:2.50,$         ;;; telluric star exposure time on the slit (sec)
    tel_magv:6.21,$         ;;; telluric V band magnitude (assuming an A0V spectum)
    tel_output:1,$
    outfile_merged_telluric: outp_path+telname+'_20180101_merged_corr.fits'$
}

run_mage_pipeline,conf_str,tel_conf_str=tel_conf_str,/plot

end
