pro run_mage_pipeline,conf_str,tel_conf_str=tel_conf_str,plot=plot

    pipeline_version = 'CfA MagE pipeline v1.0.0rc6-20210419'
    t0=systime(1)
    debug=1

    flat_file=conf_str.flat_file
    xe_file=conf_str.xe_file
    arc_file=conf_str.arc_file
    dayarc_file=(tag_exist(conf_str,'dayarc_file'))? conf_str.dayarc_file : ''

    obj_file=conf_str.obj_file
    sky_mask=conf_str.sky_mask
    outfile_merged=conf_str.outfile_merged
    outfile_merged_corr=conf_str.outfile_merged_corr
    outfile_orders=conf_str.outfile_orders
    raw2doutput=(tag_exist(conf_str,'raw_2d_output'))? conf_str.raw_2d_output : 0
    outfile_orders_raw=(tag_exist(conf_str,'outfile_orders_raw'))? conf_str.outfile_orders_raw : strmid(outfile_orders,0,strpos(outfile_orders,'.fits',/reverse_search))+'_raw.fits'

    sky_file=(tag_exist(conf_str,'sky_file'))? conf_str.sky_file : conf_str.obj_file
    sky_exp_weights=(tag_exist(conf_str,'sky_exp_weights'))? conf_str.sky_exp_weights : 1.0
    sky_normexp=(tag_exist(conf_str,'sky_normexp'))? conf_str.sky_normexp : -1.0
    sub_sky=(tag_exist(conf_str,'sub_sky'))? conf_str.sub_sky : 0

    flux_corr=(tag_exist(conf_str,'flux_corr'))? conf_str.flux_corr : 0
    corr_tel=(tag_exist(conf_str,'corr_tel'))? conf_str.corr_tel : 0
    cr_reject=(tag_exist(conf_str,'cr_reject'))? conf_str.cr_reject : 0

    if((corr_tel eq 1) and (n_elements(tel_conf_str) eq 1)) then begin
        tel_file=tel_conf_str.tel_file
        tel_arc_file=tel_conf_str.tel_arc_file
        tel_arc_sci=tel_conf_str.tel_arc_sci
        tel_sci_frame=tel_conf_str.tel_sci_frame
        sky_mask_tel=tel_conf_str.sky_mask_tel
        sub_sky_tel=tel_conf_str.sub_sky_tel
        tel_slit_reg=tel_conf_str.tel_slit_reg
        tel_vr0=tel_conf_str.tel_vr0
        tel_vsini0=tel_conf_str.tel_vsini0
        tel_spec_list=tel_conf_str.tel_spec_list
        tel_novsini=tel_conf_str.tel_novsini
        tel_gausslosvd=tel_conf_str.tel_gausslosvd
        tel_texp=(tag_exist(tel_conf_str,'tel_texp'))? tel_conf_str.tel_texp : 1.0
        tel_magv=(tag_exist(tel_conf_str,'tel_magv'))? tel_conf_str.tel_magv : 5.00
        output_telluric=(tag_exist(tel_conf_str,'tel_output'))? tel_conf_str.tel_output : 0
        outfile_merged_telluric=(tag_exist(tel_conf_str,'outfile_merged_telluric'))? tel_conf_str.outfile_merged_telluric : strmid(outfile_merged_corr,0,strpos(outfile_merged_corr,'.fits',/reverse_search))+'_telluric.fits'
    endif


    pixflat=1d ;; readfits('calib_FIRE/detector/H2RG/pixflat.fits',/silent)
    illum_corr_ord=1d ;; readfits('calib_FIRE/slits/illumination_orders_e0_45.fits',/silent)

    ;;;;; TRIMMING OVERSCAN AND DOING BASIC PRE-PROCESSING
    n_flat=n_elements(flat_file)
    flat_cube=fltarr(2048,1024,n_flat)
    for i=0,n_flat-1 do flat_cube[*,*,i]=(mage_subtract_overscan(readfits(flat_file[i],h_flat,/silent)))/pixflat
    flat=(n_flat gt 1)? median(flat_cube,dim=3) : flat_cube[*,*,0]
    flat=mage_convert_counts(flat,h_flat,rdnoise=rdnoise_flat)*n_flat

    n_arc=n_elements(arc_file)
    arc_cube=fltarr(2048,1024,n_arc)
    for i=0,n_arc-1 do arc_cube[*,*,i]=mage_subtract_overscan(readfits(arc_file[i],h_arc,/silent))/pixflat
    arc_thar=(n_arc gt 1)? median(arc_cube,dim=3) : arc_cube[*,*,0]
    arc_thar=mage_convert_counts(arc_thar,h_arc,rdnoise=rdnoise_arc)*n_arc

    dayarc_flag=0
    if(dayarc_file[0] ne '') then begin
        n_dayarc=n_elements(dayarc_file)
        dayarc_cube=fltarr(2048,1024,n_dayarc)
        for i=0,n_dayarc-1 do dayarc_cube[*,*,i]=mage_subtract_overscan(readfits(dayarc_file[i],h_dayarc,/silent))/pixflat
        dayarc_thar=(n_dayarc gt 1)? median(dayarc_cube,dim=3) : dayarc_cube[*,*,0]
        dayarc_thar=mage_convert_counts(dayarc_thar,h_dayarc,rdnoise=rdnoise_dayarc)
        dayarc_flag=1
    endif

    xyoff_flat=(dayarc_flag eq 1)? mage_measure_frame_offset(arc_thar,dayarc_thar,/verbose,xshift=3,yshift=6,magnification=20) : [0.0,0.0]

    n_xeflat=n_elements(xe_file)
    xe_cube=fltarr(2048,1024,n_xeflat)
    for i=0,n_xeflat-1 do xe_cube[*,*,i]=mage_subtract_overscan(readfits(xe_file[i],h_xe,/silent))
    xe_flat=(n_xeflat gt 1)? median(xe_cube,dim=3) : xe_cube[*,*,0]
    xe_flat=mage_convert_counts(xe_flat,h_xe,rdnoise=rdnoise_xe)

    obj1=(mage_subtract_overscan(readfits(obj_file,h_obj)))/pixflat
    sxaddpar,h_obj,'XOFFFLAT',xyoff_flat[0],' X offset between flat and science (measured with ThAr)'
    sxaddpar,h_obj,'YOFFFLAT',xyoff_flat[1],' Y offset between flat and science (measured with ThAr)'
    sxaddpar,h_obj,'SOFTWARE',pipeline_version

    mage_update_fits_header,h_obj

    obj1=mage_convert_counts(obj1,h_obj,rdnoise=rdnoise)
    gain=1.0
    if(cr_reject eq 1) then begin
        ; gain=sxpar(h_obj,'EGAIN')
        ; if(gain eq 0) then gain=1.0
        ; rdnoise=sxpar(h_obj,'ENOISE')
        ; if(rdnoise eq 0) then rdnoise=3.0
        la_cosmic_array,obj1,noise=sqrt((obj1>0) + rdnoise^2)/sqrt(gain),gain=gain,readn=rdnoise,maskarr=mask_cr,sigc=4.
        crpix=where(mask_cr ne 0, ccrpix)
        if(ccrpix gt 0) then obj1[crpix]=!values.f_nan
    endif

    if(sub_sky eq 1) then begin
        n_sky_file=n_elements(sky_file)
        sky_cube=fltarr(2048,1024,n_sky_file)
        if(n_elements(sky_exp_weights) ne n_sky_file) then sky_exp_weights=dblarr(n_sky_file)+1d/n_sky_file
        exp_sky=0.0
        for i=0,n_sky_file-1 do begin
            sky_cur=mage_subtract_overscan(readfits(sky_file[i],h_sky,/silent))
            sky_cur=mage_convert_counts(sky_cur,h_sky,rdnoise=rdnoise_sky)
            sky_cube[*,*,i]=sky_cur*sky_exp_weights[i]
            exp_sky+=sxpar(h_sky,'EXPTIME')
        endfor
        if(sky_normexp lt 0) then begin
            sky_normexp=sxpar(h_obj,'EXPTIME')/exp_sky*n_sky_file
            print,'Sky exposure normalization factor:',sky_normexp
        endif
        sky1=(n_sky_file gt 1)? total(sky_cube,3)*sky_normexp : sky_cube[*,*,0]*sky_normexp
    endif else sky1=obj1*0.0

    ;; trimming the top (bad) line of the detector
    flat[*,0]=flat[*,1]
    xe_flat[*,0]=xe_flat[*,1]

    ;;;;; FITTING WAVELENGTH SOLUTION
    clay=(sxpar(h_obj,'TELESCOP') eq 'Clay_Mag_2')? 1 : 0
    arc_detect_fwhm=(sxpar(h_obj,'SLITNAME') eq '0.50')? 2.0 : 3.0
    k_flat=median(flat[1024-100:1024+100,868-100:868+100])
    k_xe_flat=median(xe_flat[1024-200:1024+200,400-100:400+100])
    kwl3d=mage_fit_wlsol(arc_thar/(sxpar(h_arc,'EXPTIME')*0.4)/n_arc, shift_image(flat+xe_flat*k_flat/k_xe_flat*10.0,xyoff_flat[0],xyoff_flat[1]),display=plot,trace_coeff=tr_c,deg_fit=[9,6,1],clay=clay,detect_fwhm=arc_detect_fwhm,domeflat=shift_image(flat,xyoff_flat[0],xyoff_flat[1]))

    ;;;;; MODELLING SCATTERED LIGHT
    flat_sc=mage_model_sc_light(shift_image(flat,xyoff_flat[0],xyoff_flat[1]),tr_c,/bspl,gain=1d,rdnoise=rdnoise_flat) ; deg1=10,deg2=6,/max)
    flat_sub=(shift_image(flat,xyoff_flat[0],xyoff_flat[1])-flat_sc)
    flat_norm_coeff=max(median((flat_sub)[1100:1500,600:900],9))
    flat_norm=flat_sub/flat_norm_coeff
    n_ord=n_elements(tr_c[0,*])

    xe_flat_sc=mage_model_sc_light(shift_image(xe_flat,xyoff_flat[0],xyoff_flat[1]),tr_c,/bspl,gain=1d,rdnoise=rdnoise_xe) ;,deg1=10,deg2=6,/max,gain=1d,rdnoise=rdnoise_xe) ;,/bspl)
    xe_flat1=shift_image(xe_flat,xyoff_flat[0],xyoff_flat[1])-xe_flat_sc

    obj1sc=mage_model_sc_light(obj1,tr_c,deg1=10,deg2=6,/max,gain=1d,rdnoise=rdnoise) ;,/bspl)
    obj1=obj1-obj1sc

    xcrd=dindgen(2048) # (dblarr(1024)+1d)
    ycrd=(dblarr(2048)+1d) # dindgen(1024)
    ncrd=mage_xy2nl(xcrd,ycrd,tr_c,l=lcrd,dl=dlcrd,/truen)
    lcrdn=lcrd ;;*0d +0.5d

    good_map=where(ncrd gt 0,cgood_map)
    wl_gm=poly3d(double(ncrd[good_map]),xcrd[good_map],lcrd[good_map],kwl3d,/irreg)
    wlmap=dblarr(2048,1024)+!values.d_nan
    wlmap[good_map]=wl_gm
    c_logdqe_wl=[7.2424158d,-0.0025172502d,2.5239007d-07,-8.5559854d-12] ;; empirical approximation of log10(dqe) as a function of wl(A), which we use to "flatten" the flat
    flat_cmap=10d^poly(wlmap,c_logdqe_wl)/0.092
    flat_cmap_yp1=shift(flat_cmap,0,1)
    flat_cmap_ym1=shift(flat_cmap,0,-1)
    yp1_flat_cmap=where(finite(flat_cmap_yp1) eq 1 and finite(flat_cmap) ne 1, cyp1_flat_cmap)
    ym1_flat_cmap=where(finite(flat_cmap_ym1) eq 1 and finite(flat_cmap) ne 1, cym1_flat_cmap)
    if(cyp1_flat_cmap gt 0) then flat_cmap[yp1_flat_cmap]=flat_cmap_yp1[yp1_flat_cmap]
    if(cym1_flat_cmap gt 0) then flat_cmap[ym1_flat_cmap]=flat_cmap_ym1[ym1_flat_cmap]

    if(sub_sky eq 1) then begin
        sky1sc=total(sky_exp_weights)*sky_normexp*mage_model_sc_light(sky1/(total(sky_exp_weights)*sky_normexp),tr_c,deg1=10,deg2=6,/max,gain=1d,rdnoise=rdnoise)  ;/bspl)
        isky_img= gain^2 / (gain *abs(sky1/(total(sky_exp_weights)*sky_normexp)) + rdnoise^2)
        sky1=sky1-sky1sc
        sky1_model=total(sky_exp_weights)*sky_normexp*create_sky_echelle_order(sky1/(total(sky_exp_weights)*sky_normexp),isky_img=isky_img,wlmap,lcrd,ncrd,sky_mask=sky_mask,everyn=30,6+indgen(n_ord),dim=1,rdnoise=rdnoise,gain=gain) ;,npoly=1)
    endif else sky1_model=0.0

    obj=obj1-sky1_model
    obj_p=obj1+obj1sc

    slit_function=1d
    slit_function_2d=dblarr(2048,37)+1d

    wl_new=dblarr(2048,n_ord)
    ord_extr=dblarr(2048,n_ord)
    ord_extr_fit=dblarr(2048,n_ord)
    flat_extr=dblarr(2048,n_ord)
    ord_extr_sky=dblarr(2048,n_ord)
    extr_prof_all=dblarr(37,n_ord)
    ord_lin=dblarr(2048,37,n_ord)

    obj_extr_2d=dblarr(2048,37,n_ord)
    objsky_extr_2d=dblarr(2048,37,n_ord)
    obj_p_extr_2d=dblarr(2048,37,n_ord)
    flatn_extr_2d=dblarr(2048,37,n_ord)
    flatn_p_extr_2d=dblarr(2048,37,n_ord)
    xefl_extr_2d=dblarr(2048,37,n_ord)

    ;;;;; 2D ORDER EXTRACTION
    for i=0,n_ord-1 do begin
        obj_extr_2d[*,*,i]=mage_extract_order(obj,tr_c,i)
        objsky_extr_2d[*,*,i]=mage_extract_order(obj1,tr_c,i)
        obj_p_extr_2d[*,*,i]=mage_extract_order(obj_p,tr_c,i)
        flatn_extr_2d[*,*,i]=mage_extract_order(flat_norm*flat_cmap,tr_c,i)
        flatn_p_extr_2d[*,*,i]=mage_extract_order((flat_norm+flat_sc/flat_norm_coeff)*flat_cmap,tr_c,i)
        xefl_extr_2d[*,*,i]=mage_extract_order(xe_flat1*flat_cmap,tr_c,i)
    endfor

    ;;;;; MODELLING AND CORRECTING FRINGES
    fringe_pattern=mage_create_fringe_pattern(flatn_p_extr_2d)

    flatn_p_extr_2d_c=mage_correct_fringes(flatn_p_extr_2d,fringe_pattern,smw=100)
    flatn_extr_2d=flatn_extr_2d-(flatn_p_extr_2d-flatn_p_extr_2d_c)
    obj_p_extr_2d_c=mage_correct_fringes(obj_p_extr_2d,fringe_pattern,smw=100)
    obj_extr_2d=obj_extr_2d-(obj_p_extr_2d-obj_p_extr_2d_c)
    objsky_extr_2d=objsky_extr_2d-(obj_p_extr_2d-obj_p_extr_2d_c)
    xefl_extr_2d=mage_correct_fringes(xefl_extr_2d,fringe_pattern,smw=100)

    ;;;;; CREATING SYNTHETIC FLAT FROM QUARTZ+XE
    flat_synth_2d=mage_create_synth_flat(flatn_extr_2d,xefl_extr_2d,npoly=1,bkspace=192,trans_ord=4,$
        second_trans_ord=5,second_ord_synth_flatn=flat_synth_2d_5o,nopixvar=clay) ;;,trans_ord=3,trans_start=450,trans_end=750)
    ;;;;; TELLURIC CORRECTION
    if(keyword_set(corr_tel)) then begin
        tel1=(mage_subtract_overscan(readfits(tel_file,h_tel)))/pixflat
        tel1=mage_convert_counts(tel1,h_tel,rdnoise=rdnoise_etel)
        sxaddpar,h_tel,'SOFTWARE',pipeline_version
        mage_update_fits_header,h_tel

        if(~keyword_set(tel_arc_sci)) then begin
            n_tel_arc=n_elements(tel_arc_file)
            tel_arc_cube=fltarr(2048,1024,n_tel_arc)
            for i=0,n_tel_arc-1 do tel_arc_cube[*,*,i]=mage_subtract_overscan(readfits(tel_arc_file[i],h_tel_arc,/silent))
            tel_arc_thar=(n_tel_arc gt 1)? median(tel_arc_cube,dim=3) : tel_arc_cube[*,*,0]
            tel_arc_thar=mage_convert_counts(tel_arc_thar,h_arc,rdnoise=rdnoise_tel_arc)*n_tel_arc

            xyoff_flat_tel=(dayarc_flag eq 1)? mage_measure_frame_offset(tel_arc_thar,dayarc_thar,/verbose,xshift=3,yshift=6,magnification=20) : [0.0,0.0]
            sxaddpar,h_tel,'XOFFFLAT',xyoff_flat[0],' X offset between flat and star (measured with ThAr)'
            sxaddpar,h_tel,'YOFFFLAT',xyoff_flat[1],' Y offset between flat and star (measured with ThAr)'

            ;;kwl3d_tel=mage_fit_wlsol(tel_arc_thar/(sxpar(h_tel_arc,'EXPTIME')*0.4), flat+xe_flat*k_flat/k_xe_flat*10.0,display=plot,trace_coeff=tr_c_tel,deg_fit=[8,6,1],clay=clay,detect_fwhm=arc_detect_fwhm,domeflat=flat)
            kwl3d_tel=mage_fit_wlsol(tel_arc_thar/(sxpar(h_tel_arc,'EXPTIME')*0.4)/n_tel_arc, shift_image(flat+xe_flat*k_flat/k_xe_flat*10.0,xyoff_flat_tel[0],xyoff_flat_tel[1]),display=plot,trace_coeff=tr_c_tel,deg_fit=[9,6,1],clay=clay,detect_fwhm=arc_detect_fwhm,domeflat=shift_image(flat,xyoff_flat_tel[0],xyoff_flat_tel[1]))
 
            tel_wl_gm=poly3d(double(ncrd[good_map]),xcrd[good_map],lcrd[good_map],kwl3d_tel,/irreg)
            wlmap_tel=dblarr(2048,1024)+!values.d_nan
            wlmap_tel[good_map]=tel_wl_gm
        endif else begin
            kwl3d_tel=kwl3d
            wlmap_tel=wlmap
        endelse

        if(tel_sci_frame ne 1) then begin
            tel1sc=mage_model_sc_light(tel1,tr_c_tel,deg1=10,deg2=6,/max,gain=1d,rdnoise=rdnoise_tel) ;,/bspl)
            tel1=tel1-tel1sc
        endif else begin
            tel1sc=obj1sc
            tel1=obj1
        endelse

        tel_p=tel1+tel1sc
        if(sub_sky_tel eq 1) then begin
            if(n_elements(sky_mask_tel) eq 1) then begin
                if(sky_mask_tel[0] eq -1) then sky_mask_tel=mage_estimate_sky_mask(tel1,tr_c_tel) ;,nsky=13)
            endif
            isky_tel_img= gain^2 / (gain *abs(tel_p) + rdnoise_tel^2)

            tel_sky1_model=create_sky_echelle_order(tel1,isky_img=isky_tel_img,wlmap_tel,lcrd,ncrd,sky_mask=sky_mask_tel,everyn=20,6+indgen(n_ord),dim=1,npoly=1,gain=1.0,rdnoise=rdnoise_tel)
        endif else tel_sky1_model=0.0
        tel=tel1-tel_sky1_model

        tel_extr_2d=dblarr(2048,37,n_ord)
        tel_p_extr_2d=dblarr(2048,37,n_ord)
        for i=0,n_ord-1 do begin
            tel_extr_2d[*,*,i]=mage_extract_order(tel,tr_c_tel,i)
            tel_p_extr_2d[*,*,i]=mage_extract_order(tel_p,tr_c_tel,i)
        endfor

        tel_p_extr_2d_c=mage_correct_fringes(tel_p_extr_2d,fringe_pattern,smw=100)
        tel_extr_2d=tel_extr_2d-(tel_p_extr_2d-tel_p_extr_2d_c)

        tel_lin_2d=dblarr(2048,37,n_ord)
        flatn_lin_2d=dblarr(2048,37,n_ord)
        tel_lin_2d_5o=dblarr(2048,37,n_ord)
        flatn_lin_2d_5o=dblarr(2048,37,n_ord)
        tel_first_order=3
        wl_ord=wl_new
        wl_ord_nm=wl_new
        for i=0,n_ord-1 do begin
            tel_lin_2d[*,*,i]=mage_order_linearisation(tel_extr_2d[*,*,i]/flat_synth_2d[*,*,i]/illum_corr_ord/slit_function_2d,i,kwl3d_tel,wl,/autowl)
            flatn_lin_2d[*,*,i]=mage_order_linearisation(flat_synth_2d[*,*,i]*illum_corr_ord*slit_function_2d,i,kwl3d_tel,wl,/autowl)
            flatn_lin_2d_5o[*,*,i]=mage_order_linearisation(flat_synth_2d_5o[*,*,i]*illum_corr_ord*slit_function_2d,i,kwl3d_tel,wl,/autowl)
            wl_new[*,i]=wl
            wl_ord[*,i]=wl
            wl_ord_nm[*,i]=wl_ord[*,i]/10d
        endfor

        tel_extr=total(tel_lin_2d[*,tel_slit_reg,*],2)
        tel_extr[2005:*,*]=!values.f_nan
        tel_airmass=sxpar(h_tel,'AIRMASS')
        obj_airmass=sxpar(h_obj,'AIRMASS')
        swln=1
        fix_lsf_sig_arr=bytarr(n_ord)+1
        fix_lsf_sig_arr[n_ord-4:*]=0
        mwl_w=min(abs(wl_new[1024,*]-8800.0),ord_lsf_par)
        tel_res0=fire_get_wloffset_telluric(wl_ord_nm[*,tel_first_order:*],$
            tel_extr[*,tel_first_order:*],maxwl_global=1052.,/no_lsf_sig,mdeg=15,airmass=tel_airmass,$
            vr0=tel_vr0,vsini0=tel_vsini0,plot=plot,/mage,/fix_pwv,ord_lsf_par=ord_lsf_par-tel_first_order,ord_vel_par=ord_lsf_par-tel_first_order,fix_lsf_sig=fix_lsf_sig_arr,swln=swln,/flag_swl,diff_airmass=obj_airmass-tel_airmass,$
            star_list=tel_spec_list,novsini=keyword_set(tel_novsini),gausslosvd=keyword_set(tel_gausslosvd)) ;,/fit_vsi)
        tel_res0upd=fire_adjust_wloffset_telluric(tel_res0,deg1=0,deg2=1,/robust)
        tel_res0upd[n_ord-tel_first_order-2]=tel_res0[n_ord-tel_first_order-2] ;;; keep the correction for the 7600A order
        for i=0,tel_first_order-1 do wl_ord[*,i]-=(swln eq 1)? tel_res0upd[0].wl_shift.wl_shift[0]*10d : interpol(tel_res0upd[0].wl_shift.wl_shift*10d,tel_res0upd[0].wl_shift.wl_coord*10d,wl_ord[*,tel_first_order],quadratic=(swln eq 3),spline=(swln gt 4)) 
        for i=tel_first_order,n_ord-1 do wl_ord[*,i]-=(swln eq 1)? tel_res0upd[i-tel_first_order].wl_shift.wl_shift[0]*10d : interpol(tel_res0upd[i-tel_first_order].wl_shift.wl_shift*10d,tel_res0upd[i-tel_first_order].wl_shift.wl_coord*10d,wl_ord[*,i],quadratic=(swln eq 3),spline=(swln gt 4)) 

        for i=0,n_ord-1 do begin
            tel_lin_2d[*,*,i]=mage_order_linearisation(tel_extr_2d[*,*,i]/flat_synth_2d[*,*,i]/illum_corr_ord/slit_function_2d,i,kwl3d_tel,wl_ord[*,i])
            tel_lin_2d_5o[*,*,i]=mage_order_linearisation(tel_extr_2d[*,*,i]/flat_synth_2d_5o[*,*,i]/illum_corr_ord/slit_function_2d,i,kwl3d_tel,wl_ord[*,i])
        endfor
        blaze_off_tel=mage_estimate_blaze_offset(tel_lin_2d,flatn_lin_2d,wl_new,/lin,xmin=-50,xmax=50)
        blaze_off_tel_5o=mage_estimate_blaze_offset(tel_lin_2d_5o,flatn_lin_2d_5o,wl_new,/lin,xmin=-50,xmax=50)

        tel_extr_corr=total(tel_lin_2d[*,tel_slit_reg,*],2)
        tel_extr_corr[2005:*,*]=!values.f_nan
        star_weights=-1d
        tel_res=fire_get_wloffset_telluric(wl_ord_nm[*,tel_first_order:*],$
            tel_extr_corr[*,tel_first_order:*],maxwl_global=1052.,/no_lsf_sig,mdeg=15,airmass=tel_airmass,$
            vr0=tel_vr0,vsini0=tel_vsini0,plot=plot,/mage,/fix_pwv,/fix_lsf_sig,ord_lsf_par=ord_lsf_par-tel_first_order,ord_vel_par=ord_lsf_par-tel_first_order,swln=0,diff_airmass=obj_airmass-tel_airmass,$
            star_list=tel_spec_list,novsini=keyword_set(tel_novsini),gausslosvd=keyword_set(tel_gausslosvd),star_weights=star_weights)

        sxaddpar,h_tel,'VR',float(tel_res[0].p_tel[0]),' [km/s] radial velocity from telluric correction'
        sxaddpar,h_tel,'VSINI',float(tel_res[0].p_tel[1]),' [km/s] rotational broadening from telluric correction'
        pos_wgt=where(star_weights gt 0, cpos_wgt)
        for k=0,cpos_wgt-1 do begin
            sxaddpar,h_tel,'TELTMP'+string(k,format='(i2.2)'),strmid(tel_spec_list[pos_wgt[k]],strpos(tel_spec_list[pos_wgt[k]],'/',/reverse_search)+1),' telluric template spectrum'
            sxaddpar,h_tel,'TELWGT'+string(k,format='(i2.2)'),star_weights[pos_wgt[k]],' telluric template weight coefficient'
        endfor

        tel_res_final=[replicate(tel_res[0],tel_first_order),tel_res]
        tel_res_final[0:tel_first_order-1].tellcorr[*]=1d
        tel_res_final[0:tel_first_order-1].mcont[*]=1d
        tel_res_final[0:tel_first_order-1].order=19-lindgen(tel_first_order)

        tel_res_shift=[replicate(tel_res0upd[0],tel_first_order),tel_res0upd]
        for i=0,tel_first_order-1 do tel_res_shift[i].wl_shift.wl_coord=wl_ord[fix((findgen(swln)+0.5)*2048/swln)-1,i]/10d
        tel_res_shift[0:tel_first_order-1].tellcorr[*]=1d
        tel_res_shift[0:tel_first_order-1].mcont[*]=1d
        tel_res_shift[0:tel_first_order-1].order=19-lindgen(tel_first_order)

        tel_struct = replicate({wl:wl_new[*,0],transmission:dblarr(2048)+1d},n_ord)
        tel_struct.wl=wl_new
        tel_struct.transmission = tel_res_final.tellcorr/tel_res_final.mcont

        if(keyword_set(flux_corr)) then begin
            tel_res_flux_cur=fire_get_wloffset_telluric(wl_ord_nm[*,tel_first_order:*],$
                tel_extr_corr[*,tel_first_order:*],maxwl_global=1052.,/no_lsf_sig,mdeg=15,airmass=tel_airmass,$
                vr0=tel_vr0,vsini0=tel_vsini0,plot=plot,/mage,/fix_pwv,ord_lsf_par=ord_lsf_par-tel_first_order,ord_vel_par=ord_lsf_par-tel_first_order,/fix_lsf_sig,swln=0,diff_airmass=0.0,$
                star_list=tel_spec_list,novsini=keyword_set(tel_novsini),gausslosvd=keyword_set(tel_gausslosvd))
            tel_res_flux=tel_res_final
            tel_res_flux[tel_first_order:*]=tel_res_flux_cur
            tel_struct_flux=tel_struct
            tel_struct_flux.transmission = tel_res_flux.tellcorr/tel_res_flux.mcont
        endif

    endif

    obj_lin_2d=dblarr(2048,37,n_ord)
    objsky_lin_2d=dblarr(2048,37,n_ord)
    objsky_lin_2d_5o=dblarr(2048,37,n_ord)
    recompute_flat_lin = ((n_elements(flatn_lin_2d) ne n_elements(obj_lin_2d)) or (n_elements(flatn_lin_2d_5o) ne n_elements(obj_lin_2d)))
    if(recompute_flat_lin eq 1) then begin
        flatn_lin_2d=dblarr(2048,37,n_ord)
        flatn_lin_2d_5o=dblarr(2048,37,n_ord)
    endif
    if(keyword_set(raw2doutput)) then obj_lin_2d_raw=dblarr(2048,37,n_ord)
    for i=0,n_ord-1 do begin 
        if(keyword_set(corr_tel)) then wl=wl_ord[*,i]
        obj_lin_2d[*,*,i]=mage_order_linearisation(obj_extr_2d[*,*,i]/flat_synth_2d[*,*,i]/illum_corr_ord/slit_function_2d,i,kwl3d,wl,autowl=1-keyword_set(corr_tel))
        objsky_lin_2d[*,*,i]=mage_order_linearisation(objsky_extr_2d[*,*,i]/flat_synth_2d[*,*,i]/illum_corr_ord/slit_function_2d,i,kwl3d,wl,autowl=1-keyword_set(corr_tel))
        objsky_lin_2d_5o[*,*,i]=mage_order_linearisation(objsky_extr_2d[*,*,i]/flat_synth_2d_5o[*,*,i]/illum_corr_ord/slit_function_2d,i,kwl3d,wl,autowl=1-keyword_set(corr_tel))
        if(recompute_flat_lin eq 1) then begin
            flatn_lin_2d[*,*,i]=mage_order_linearisation(flat_synth_2d[*,*,i]*illum_corr_ord*slit_function_2d,i,kwl3d_tel,wl,autowl=1-keyword_set(corr_tel))
            flatn_lin_2d_5o[*,*,i]=mage_order_linearisation(flat_synth_2d_5o[*,*,i]*illum_corr_ord*slit_function_2d,i,kwl3d_tel,wl,autowl=1-keyword_set(corr_tel))
        endif
        if(keyword_set(raw2doutput)) then obj_lin_2d_raw[*,*,i]=mage_order_linearisation(obj_extr_2d[*,*,i],i,kwl3d,wl,autowl=1-keyword_set(corr_tel))
        if(~keyword_set(corr_tel)) then wl_new[*,i]=wl
    endfor

    blaze_off=mage_estimate_blaze_offset(objsky_lin_2d,flatn_lin_2d,wl_new,/lin,xmin=-50,xmax=50)
    blaze_off_5o=mage_estimate_blaze_offset(objsky_lin_2d_5o,flatn_lin_2d_5o,wl_new,/lin,xmin=-50,xmax=50)


    flatn_lin_extr=reform(flatn_lin_2d[*,18,*],2048,n_ord)
    ord_corr_tel=mage_estimate_order_correction(flatn_lin_extr,[{blaze_offset:blaze_off_tel,ord_flat_stitch:19-4},{blaze_offset:blaze_off_tel_5o,ord_flat_stitch:19-5}],wl_ord,/lin)
    ord_corr_sci=mage_estimate_order_correction(flatn_lin_extr,[{blaze_offset:blaze_off,ord_flat_stitch:19-4},{blaze_offset:blaze_off_5o,ord_flat_stitch:19-5}],wl_ord,/lin)

    extr_fit_all=dblarr(2048,37,n_ord)+1d/37

    fcorr_vec=[0,0]

    ;;;;; MERGING ORDERS
    spec_merged_corr_tst=mage_merge_orders(obj_extr_2d/flat_synth_2d,flat_synth_2d,extr_fit_all,kwl3d,ord_corr_sci,$
        /autowl,wl=wl_m,sky_merged=sky_merged,spec_2d_merged=spec_2d_merged_corr_tst,$
        specerr_2d_merged=specerr_2d_merged_corr_tst,flatn_thr=0.0002,flatn_sthr=0.02,spec_p_extr_2d=obj_p_extr_2d_c)

    if(keyword_set(corr_tel)) then begin
        spec_merged_corr_tst=mage_merge_orders(obj_extr_2d/flat_synth_2d,flat_synth_2d,$
            extr_fit_all,kwl3d,ord_corr_sci,/autowl,wl=wl_m,sky_merged=sky_merged,$
            spec_2d_merged=spec_2d_merged_corr_tell_tst,specerr_2d_merged=specerr_2d_merged_corr_tell_tst,$
            flatn_thr=0.0002,flatn_sthr=0.02,ord_wl_shift=tel_res_shift.wl_shift,/ord_wl_nm,$
            spec_p_extr_2d=obj_p_extr_2d_c,/correct_telluric,tell=tel_struct)

        spec_2d_corr_tst=congrid(reform(spec_2d_merged_corr_tell_tst,n_elements(wl_m),37,1),n_elements(wl_m),37,2)
        spec_2d_corr_tst[*,*,1]=specerr_2d_merged_corr_tell_tst

        if(keyword_set(flux_corr)) then begin
            spec_merged_corr_tst=mage_merge_orders(tel_extr_2d/flat_synth_2d,flat_synth_2d,$
                extr_fit_all,kwl3d_tel,ord_corr_tel,wl=wl_m,sky_merged=sky_merged,$
                spec_2d_merged=spec_2d_merged_stdstar,specerr_2d_merged=specerr_2d_merged_stdstar,$
                flatn_thr=0.0002,flatn_sthr=0.02,ord_wl_shift=tel_res_shift.wl_shift,$
                /ord_wl_nm,/correct_telluric,tell=tel_struct_flux,spec_p_extr_2d=tel_p_extr_2d_c)
            fcorr_vec=mage_estimate_spectral_response(spec_2d_merged_stdstar,wl_m,tel_slit_reg,$
                error2d=specerr_2d_merged_stdstar,v0=tel_res_flux[0].p_tel[0],vsini=tel_res_flux[0].p_tel[1],$
;                pix_trans=4000+2000*clay,$
                star_list=tel_spec_list,star_weights=star_weights,plot=plot,texp=tel_texp,magv=tel_magv,airmass=((sxpar(h_tel,'AIRMASS'))>1))
            spec_2d_corr_stdstar=congrid(reform(spec_2d_merged_stdstar,n_elements(wl_m),37,1),n_elements(wl_m),37,2)
            spec_2d_corr_stdstar[*,*,1]=specerr_2d_merged_stdstar
        endif
        mage_fits_output_mef,outfile_merged_corr,spec_2d_corr_tst,congrid(reform(wl_m,n_elements(wl_m),1),n_elements(wl_m),2),h_obj,/flux,fcorr_vec=fcorr_vec
        if(keyword_set(output_telluric)) then begin
            mage_fits_output_mef,outfile_merged_telluric,spec_2d_corr_stdstar,congrid(reform(wl_m,n_elements(wl_m),1),n_elements(wl_m),2),h_tel,/flux,fcorr_vec=fcorr_vec,texp=tel_texp
            if(keyword_set(flux_corr)) then mwrfits,fcorr_vec,outfile_merged_telluric
            if(keyword_set(debug)) then mwrfits,{wave:wl_ord,tel_lin_2d:tel_lin_2d,flatn_lin_2d:flatn_lin_2d},outfile_merged_telluric
            ; plot,wl_m,smooth(total(spec_2d_merged_stdstar[*,0:31],2)*(wl_m/1d4)^7,15,/nan),xs=1,yr=[2000,20000],/ylog,/xlog,ys=1,xr=[4800,6600]
            ; jjj={wave:wl_ord,tel_lin_2d:tel_lin_2d,flatn_lin_2d:flatn_lin_2d}
            ; for i=0,12 do oplot,jjj.wave[*,i],(total((jjj.tel_lin_2d)[*,tel_slit_reg,i],2)/ord_corr_tel[i].correction_vector_lin*((jjj.wave[*,i])/1d4)^7),col=i*45+70,thick=3
            ;; for i=0,12 do oplot,jjj.wave[*,i],(total((jjj.tel_lin_2d*jjj.flatn_lin_2d/shift(jjj.flatn_lin_2d,-xyoff_flat_tel[0]*1./(7.0+19-i)*600,0,0))[*,tel_slit_reg,i],2)*((jjj.wave[*,i])/1d4)^7),col=i*45+70,thick=3
        endif
    endif

    ;;;;; WRITING OUTPUT FILES
    mage_fits_output_mef,outfile_orders,obj_lin_2d,wl_new,h_obj,/flux,fcorr_vec=fcorr_vec
    if(keyword_set(raw2doutput)) then mage_fits_output_mef,outfile_orders_raw,obj_lin_2d_raw,wl_new,h_obj,/flux,fcorr_vec=fcorr_vec

    ;;; ugly workaround to save errors
    spec_2d_tst=congrid(reform(spec_2d_merged_corr_tst,n_elements(wl_m),37,1),n_elements(wl_m),37,2)
    spec_2d_tst[*,*,1]=specerr_2d_merged_corr_tst
    mage_fits_output_mef,outfile_merged,spec_2d_tst,congrid(reform(wl_m,n_elements(wl_m),1),n_elements(wl_m),2),h_obj,/flux,fcorr_vec=fcorr_vec

    t1=systime(1)
    print,'Time elapsed: ',t1-t0,' seconds'

end
