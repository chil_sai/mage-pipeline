function mage_xy2nl,x,y,trace_coeff,x0=x0,ny=ny,l=l,dl=dl,truen=truen

s_x = size(x)
s_y = size(y)
if(n_elements(x0) ne 1) then x0=1024
if(n_elements(ny) ne 1) then ny=37

if(array_equal(s_x,s_y) ne 1) then begin
    message,/inf,'X and Y should have the same number of elements'
    return,-1
endif

n_ord=n_elements(trace_coeff[0,*,0])

n_inp=s_x[n_elements(s_x)-1]
x_vec=reform(x,n_inp)
y_vec=reform(y,n_inp)

n_vec=intarr(n_inp)-1
l_vec=dblarr(n_inp)+!values.d_nan
dl_vec=dblarr(n_inp)+!values.d_nan

order_borders = dblarr(n_inp,2)
for i=0,n_ord-1 do begin
    order_borders[*,0]=poly(x_vec-x0,trace_coeff[*,i,0])
    order_borders[*,1]=poly(x_vec-x0,trace_coeff[*,i,1])
    y_idx = where(y_vec ge order_borders[*,0] and y_vec le order_borders[*,1], cy_idx)
    if(cy_idx gt 0) then begin
        n_vec[y_idx]=i
        l_vec[y_idx]=(y_vec[y_idx]-order_borders[y_idx,0])/(order_borders[y_idx,1]-order_borders[y_idx,0])
        dl_vec[y_idx]=1d/(order_borders[y_idx,1]-order_borders[y_idx,0])
    endif
endfor

if(keyword_set(truen)) then n_vec=19-n_vec

n=(s_x[0] le 1)? n_vec : reform(n_vec,s_x[1:s_x[0]])
l=(s_x[0] le 1)? l_vec : reform(l_vec,s_x[1:s_x[0]])
dl=(s_x[0] le 1)? dl_vec : reform(dl_vec,s_x[1:s_x[0]])

return,n
end
