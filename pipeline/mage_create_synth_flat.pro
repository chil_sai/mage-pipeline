function mage_create_synth_flat,flatn,xeflat,$ ;;tr_c,kwl3d,$
    norm_coeff=norm_coeff,npoly=npoly,red_proc_order=red_proc_order,$
    trans_ord=trans_ord,trans_start=trans_start,trans_end=trans_end,$
    second_trans_ord=second_trans_ord,keep_lowflat=keep_lowflat,$
    second_ord_synth_flatn=second_ord_synth_flatn,nopixvar=nopixvar,bkspace=bkspace

    if(n_elements(trans_ord) ne 1) then trans_ord=4
    if(n_elements(trans_start) ne 1) then trans_start=800
    if(n_elements(trans_end) ne 1) then trans_end=1200
    if(n_elements(second_trans_ord) ne 1) then second_trans_ord=trans_ord
    if(n_elements(red_proc_order) ne 1) then red_proc_order=(trans_ord>second_trans_ord)+2
    s_fl=size(flatn)

    xeflat_proc=xeflat
    flatn_proc=flatn
    for i=0,(trans_ord>second_trans_ord) do xeflat_proc[*,*,i]=mage_process_flat_order(xeflat[*,*,i],bkspace=bkspace,npoly=npoly,/lowercont,/logflux)

    norm_coeff=median(xeflat_proc[trans_start:trans_end,*,trans_ord])/median(flatn[trans_start:trans_end,*,trans_ord])
    xeflat_proc/=norm_coeff
    print,'norm_coeff=',norm_coeff

    for i=(((trans_ord<second_trans_ord)-2)>0),((s_fl[3]-1) < red_proc_order) do flatn_proc[*,*,i]=mage_process_flat_order(flatn[*,*,i],bkspace=bkspace,/logflux,pixvar=flatn[*,*,i],nopixvar=nopixvar,npoly=npoly,thr_rel=1d-6)

    synth_flatn=flatn_proc
    synth_flatn[*,*,0:trans_ord-1]=xeflat_proc[*,*,0:trans_ord-1]
    w_arr=synth_flatn[*,*,trans_ord]*0d
    w_arr[trans_start:trans_end,*]=congrid(reform(dindgen(trans_end-trans_start+1)/(trans_end-trans_start),trans_end-trans_start+1,1),trans_end-trans_start+1,s_fl[2])
    w_arr[trans_end+1:*,*]=1d
    synth_flatn[*,*,trans_ord]=flatn_proc[*,*,trans_ord]*(1-w_arr)+xeflat_proc[*,*,trans_ord]*w_arr
    if(arg_present(second_ord_synth_flatn)) then begin
        if(trans_ord eq second_trans_ord) then $
            second_ord_synth_flatn=synth_flatn $
        else begin
            second_ord_synth_flatn=flatn_proc
            second_ord_synth_flatn[*,*,0:second_trans_ord-1]=xeflat_proc[*,*,0:second_trans_ord-1]
            second_ord_synth_flatn[*,*,second_trans_ord] = flatn_proc[*,*,second_trans_ord]*(1-w_arr)+xeflat_proc[*,*,second_trans_ord]*w_arr
        endelse
    endif

    if(~keyword_set(keep_lowflat)) then begin
        mask_flat=byte(synth_flatn*0)
        mask_flat[0:200,*,0]=1
        mask_flat[1450:*,*,0]=1
        mask_flat[0:200,*,1]=1
        mask_flat[1450:*,*,1]=1
        mask_flat[0:200,*,2]=1
        mask_flat[1500:*,*,2]=1
        mask_flat[0:150,*,3]=1
        mask_flat[1650:*,*,3]=1
        mask_flat[0:150,*,4]=1
        mask_flat[1700:*,*,4]=1
        mask_flat[0:150,*,5]=1
        mask_flat[1700:*,*,5]=1
        mask_flat[0:100,*,6]=1
        mask_flat[1800:*,*,6]=1
        mask_flat[0:50,*,7]=1
        mask_flat[1850:*,*,7]=1
        mask_flat[0:10,*,8]=1
        mask_flat[1950:*,*,8]=1
        b_mask=where(mask_flat eq 1)
        synth_flatn[b_mask]=!values.f_nan
        if(arg_present(second_ord_synth_flatn)) then second_ord_synth_flatn[b_mask]=!values.f_nan
    endif

    return, synth_flatn
end
