function lsf_rotate_v0_fourier, deltav, vsini, npix, v0=v0, EPSILON = epsilon
;+
; NAME:
;     LSF_ROTATE_V0_FOURIER:
;
; PURPOSE:
;     Create a Fourier image of a 1-d convolution kernel to broaden a spectrum from a rotating star
;
; EXPLANATION:
;     Can be used to derive the broadening effect (line spread function; LSF) 
;     due to  rotation on a synthetic stellar spectrum.     Assumes constant 
;     limb darkening across the disk.
;
; CALLING SEQUENCE
;     lsf = LSF_ROTATE(deltav, vsini, npix, EPSILON=)
;
; INPUT PARAMETERS:
;    deltaV - numeric scalar giving the step increment (in km/s) in the output 
;             rotation kernel.  
;    Vsini - the rotational velocity projected  along the line of sight (km/s)
;    npix  - number of pixel for the output Fourier image, defaults to 2048
;
; OUTPUT PARAMETERS:
;    LSF - The Fourier image of the convolution kernel vector for the specified rotational velocity
;          and systemic velocity v0. LSF will 
;          always be of type DOUBLE COMPLEX.
;
;          To actually compute the broadening. the spectrum should be convolved
;          with the rotational LSF. 
; OPTIONAL INPUT PARAMETERS:
;    V0  - the systemic velocity (in km/s), default is 0
;    Epsilon - numeric scalar giving the limb-darkening coefficient, 
;          default = 0.6 which is typical for  photospheric lines.    The
;          specific intensity I at any angle theta from the specific intensity
;          Icen at the center of the disk is given by:
;  
;          I = Icen*(1-epsilon*(1-cos(theta))
;                    
; EXAMPLE:
;    (1) Plot the LSF for a star rotating at 90 km/s in both velocity space and
;        for a central wavelength of 4300 A.    Compute the LSF every 3 km/s
;
;       IDL> lsf = lsf_rotate(3,90,velgrid=vel)      ;LSF will contain 61 pts
;       IDL> plot,vel,lsf                    ;Plot the LSF in velocity space
;       IDL> wgrid = 4300*(1+vel/3e5)       ;Speed of light = 3e5 km/s
;       IDL> oplot,wgrid,lsf                ;Plot in wavelength space
;
; REVISION HISTORY:
;    Written,  I. Chilingarian             April 2019
;-
    On_error,2
    compile_opt idl2
    if n_params() LT 1 then begin
         print,'Syntax - rkernel = lsf_rotate_v0_fourier(deltav, vsini)'
         print,'      Input Keyword: v0'
         print,'      Input Keyword: Epsilon'
         print,'      Output Keyword: Velgrid'
         return,-1
    endif
    if(n_params() le 1) then vsini=0d
    if(n_params() le 2) then npix=2048l
    if (n_elements(epsilon) ne 1) then epsilon=0.6d
    if (n_elements(v0) ne 1) then v0=0d
    vsini_val=(vsini > (deltav/10d))

    c=299792.458d
    x=dindgen((npix>2l)/2l)/((npix>2l)/2d)
    vspix=vsini_val/deltav

    ;; the Fourier image from Collins & Truax 1995 ApJ 439 860
    ll=6d*(1d -epsilon)/(3d -epsilon)*(beselj(x*vspix*!dpi,1)/(x*vspix*!dpi)-$
        epsilon/(1d -epsilon)*(cos(x*vspix*!dpi)/(x*vspix*!dpi)^2-sin(x*vspix*!dpi)/(x*vspix*!dpi)^3))
    ll[0]=1d

    w_f=2d*!dpi*(dindgen(npix)-npix/2+1)/npix
    rot_lsf_f= exp(alog(dcomplex([ll,reverse(ll)],dblarr(npix)))+$
                   dcomplex(dblarr(npix), shift(-(v0/deltav)*w_f,npix/2-1)))

    return, rot_lsf_f
   
end
