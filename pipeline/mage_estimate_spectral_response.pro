function mage_estimate_spectral_response,spec2d,wl,slitreg,error2d=error2d,$
    star_list=star_list,star_weights=star_weights,v0=v0,vsini=vsini,$
    magv=magv,texp=texp,airmass=airmass,pix_trans=pix_trans,plot=plot

    if(n_elements(star_list) eq 0) then $
        star_list=[$
            getenv('MAGE_PIPELINE_PATH')+'calib_MagE/stellar_templates/phoenix/lte09600-4.00-0.0.PHOENIX-ACES-AGSS-COND-2011-HiRes.fits.gz'$
        ]
    n_star=n_elements(star_list)

    if(n_elements(airmass) ne 1) then airmass=1d
    if(n_elements(texp) ne 1) then texp=1d
    if(n_elements(magv) ne 1) then magv=5.00d

    if(n_elements(star_weights) ne n_star) then star_weights=dblarr(n_star)+1d
    if(n_elements(v0) ne 1) then v0=0.0
    if(n_elements(vsini) ne 1) then vsini=0.5
    if(n_elements(pix_trans) ne 1) then pix_trans=4000 ;; 11800
    if(n_elements(bk_coarse) ne 1) then bk_coarse=60 ;;40
    if(n_elements(bk_fine) ne 1) then bk_fine=200

    for i=0,n_star-1 do begin
        star_cur=readfits(star_list[i],hstar,/silent)/1.52d24*10d^(-0.4d*(magv-0.03)) ;;; converting to Vega fluxes (erg/cm^2/s/A) and accounting for the visual magnitude of the star
        star_tmp=(i eq 0)? star_cur*star_weights[i] : star_tmp+star_cur*star_weights[i]
    endfor
    parse_spechdr,hstar,wl=wlstar,velscale=velScStar


    star_krnl=lsf_rotate_v0(velScStar,vsini,v0=-v0) 
    star_conv_tmp=convol(star_tmp,star_krnl)
    star_conv=interpol(star_conv_tmp,wlstar,wl,/spl)
    
    ext_vec=mage_calc_ext_lco(wl,airmass)
    mask_spec=byte(spec2d[*,0]*0)

    S=!pi*((650.0/2)^2-(100.0/2)^2) ;total square mirror of telescope in cm^2

    star_obs=total(spec2d[*,slitreg],2)/ext_vec/texp
    estar_obs=sqrt(total(error2d[*,slitreg]^2,2))/ext_vec/texp

    ;;counts_orig=flux_orig*S*wl_orig*1d-8/(6.625d-27*2.99792458d10)

    xx=wl
    yy=star_obs/(star_conv*S*wl*1d-8/(6.625d-27*2.99792458d10))
    wlmax=10577. ;9477.

    iv=1/estar_obs^2/(star_conv*S*wl*1d-8/(6.625d-27*2.99792458d10))
    mask_spec[where(abs(wl-6890) lt 30.0 or $
                abs(wl-3934) lt 10.0 or $
                abs(wl-3968) lt 10.0 or $
                abs(wl-4102) lt 10.0 or $
                abs(wl-4340) lt 45.0 or $
                abs(wl-4861) lt 45.0 or $
                abs(wl-5893) lt 8.0 or $
                abs(wl-6563) lt 45.0 or $
;                abs(wl-7250) lt 100.0 or $
                abs(wl-7640) lt 65.0 or $
;                abs(wl-8230) lt 20.0 or $
;                abs(wl-8980) lt  40.0 or $
;                abs(wl-9120) lt  60.0 or $
                abs(wl-9350) lt  25.0 or $
                wl gt wlmax)]=1
    good_mask=where(mask_spec eq 0, compl=bad_mask)
    g_vec=where(finite(yy+iv) eq 1 and yy gt 1d-4 and mask_spec eq 0,cg_vec,compl=b_vec)
    yy[b_vec]=interpol(yy[g_vec],xx[g_vec],xx[b_vec])
    iv[b_vec]=interpol(iv[g_vec],xx[g_vec],iv[b_vec])
    gwl=where(wl lt wlmax)
    sset1=bspline_iterfit(xx[gwl]^0.5,yy[gwl],ivar=iv[gwl]^2,nbk=bk_fine)
    dqe1=bspline_valu(wl^0.5,sset1)
    sset2=bspline_iterfit(xx[g_vec]^0.5,yy[g_vec],ivar=iv[g_vec]^2,nbk=bk_coarse)
    dqe2=bspline_valu(wl^0.5,sset2)
    dqe=dqe1
    dqe[pix_trans:*]=dqe2[pix_trans:*]

    flux_erg=1d/(dqe*S*wl*1d-8/(6.625d-27*2.99792458d10))
    flux_mJy=flux_erg*wl^2*3.3356E7

    flux_corr={wave:wl,dqe_blaze:dqe,flux_erg:flux_erg,flux_mJy:flux_mJy}

    if(keyword_set(plot)) then begin
        plot,xx,yy,xs=1,ys=1,xr=[3500,10500],/yl,yr=[0.01,1.0]
        oplot,wl,dqe,col=254,thick=3
        oplot,wl,dqe2,col=128
    endif
    return,flux_corr
end
