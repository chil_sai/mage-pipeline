function correl_images_frac,imageA,imageB,XSHIFT = x_shift,     $
                                          YSHIFT = y_shift,     $
                                          XOFFSET_B = x_offset, $
                                          YOFFSET_B = y_offset, $
                                          REDUCTION = reducf,   $
                                          MAGNIFICATION = Magf, $
                                          subbgr=subbgr,int5p=int5p

    if N_params() LT 2 then begin 
        print,'Syntax  -  Result = CORREL_IMAGES_FRAC( image_A, image_B,'
        print,'[         XSHIFT=, YSHIFT=, XOFFSET_B=, YOFFSET_B=, REDUCTION=, '
        print,'          MAGNIFICATION=  )'
        return,-1
    endif

    if(n_elements(x_shift) ne 1) then x_shift=7
    if(n_elements(y_shift) ne 1) then y_shift=7
    if(n_elements(magf) ne 1) then magf=1
    if(n_elements(x_offset) ne 1) then x_offset=0d
    if(n_elements(y_offset) ne 1) then y_offset=0d
    nx=2*x_shift*magf+1
    ny=2*y_shift*magf+1
    correl_mat=dblarr(nx,ny)
    imgA=(keyword_set(subbgr))? imageA-median(imageA) : imageA
    imgB=(keyword_set(subbgr))? imageB-median(imageB) : imageB
    totAA=total(imgA^2)
    if(totAA eq 0) then return, correl_mat

    for y=0,ny-1 do begin
        for x=0,nx-1 do begin
            dx=(x-x_shift*magf)/double(magf)+x_offset
            dy=(y-y_shift*magf)/double(magf)+y_offset
            im2=(keyword_set(int5p))? shift_5p(imgB,dx,dy) : shift_image(imgB,dx,dy)
            totBB=total(im2^2)
            correl_mat[x,y] = (totBB eq 0)? 0 : total(imgA*im2)/sqrt(totAA*totBB)
        endfor
    endfor


    return, correl_mat
end
