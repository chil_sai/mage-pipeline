function mage_process_flat_order,flat_order_inp,bkspace=bkspace,npoly=npoly,$
        nopixvar=nopixvar,pixvar_order=pixvar_order,$
        lowercont=lowercont,uppercont=uppercont,$
        sg_win2=sg_win2,smw=smw,logflux=logflux,thr_rel=thr_rel
    if(n_elements(bkspace) ne 1) then bkspace=100.
    if(n_elements(npoly) ne 1) then npoly=1
    if(n_elements(sg_win2) ne 1) then sg_win2=16
    if(n_elements(smw) ne 1) then smw=5.0
    if(n_elements(thr_rel) ne 1) then thr_rel=0.01

    flat_order = flat_order_inp
    s_ord=size(flat_order)
    x_crd=dindgen(s_ord[1]) # (dblarr(s_ord[2])+1d)
    y_crd=(dblarr(s_ord[1])+1d) # dindgen(s_ord[2])

    norm_prof=median(flat_order[s_ord[1]/2-300:s_ord[1]/2+300,*],dim=1)
    norm_prof=norm_prof/max(norm_prof,/nan)
    norm_arr=congrid(transpose(norm_prof),s_ord[1],s_ord[2])
    
    flat_order/=norm_arr

    if(keyword_set(logflux)) then flat_order=alog10(flat_order)
    g_fl=where(finite(flat_order) eq 1 and flat_order gt (keyword_set(logflux)? alog10(thr_rel) : thr_rel), cg_fl)
    if(cg_fl lt 1000) then return,flat_order
    if(n_elements(pixvar_order) ne n_elements(flat_order)) then pixvar_order=flat_order
    
    s_x = sort(x_crd[g_fl])
    xx = x_crd[g_fl[s_x]]
    yy = y_crd[g_fl[s_x]]
    ff = flat_order[g_fl[s_x]]

    sset = (npoly eq 0)? $
        bspline_iterfit(xx, ff, ivar=(dblarr(cg_fl)+1d), bkspace=bkspace) :$
        bspline_iterfit(xx, x2=yy, ff, ivar=(dblarr(cg_fl)+1d), npoly=npoly, bkspace=bkspace)
    flat_model = (npoly eq 0)? $
        reform(bspline_valu(reform(x_crd,s_ord[1]*s_ord[2]),sset),s_ord[1],s_ord[2]) :$
        reform(bspline_valu(reform(x_crd,s_ord[1]*s_ord[2]),sset,x2=reform(y_crd,s_ord[1]*s_ord[2])),s_ord[1],s_ord[2])

    if(keyword_set(lowercont) or keyword_set(uppercont)) then begin
        flat_tmp = flat_order/flat_model
        flat_tmp_mid=smooth(flat_tmp[*,s_ord[2]/2],smw,/nan)
        gpp=where(finite(flat_tmp_mid) eq 1, cgpp)
        xxx=lindgen(s_ord[1])
        sg_1 = savgol(sg_win2,sg_win2,1,4)
        flat_d1=interpol(convol(flat_tmp_mid[gpp],sg_1,/edge_trunc),gpp,xxx)
        zero_d1=(keyword_set(lowercont))? $
            where(flat_d1[1:*] gt 0 and flat_d1[0:s_ord[1]-2] lt 0 and flat_tmp_mid[1:*] lt 1,czero_d1) : $
            where(flat_d1[1:*] lt 0 and flat_d1[0:s_ord[1]-2] gt 0 and flat_tmp_mid[1:*] gt 1,czero_d1)
        if(czero_d1 gt 4) then for y=0,s_ord[2]-1 do flat_model[*,y]=interpol((smooth(flat_order[*,y],smw,/nan))[zero_d1],zero_d1,xxx,/spl)
    endif

    if(keyword_set(logflux)) then flat_model=10d^flat_model
    flat_order_proc=flat_model*norm_arr

    if(~keyword_set(nopixvar)) then $
        for y=0,s_ord[2]-1 do flat_order_proc[*,y]+=(pixvar_order[*,y]-smooth(pixvar_order[*,y],smw,/nan))

    return, flat_order_proc
end
