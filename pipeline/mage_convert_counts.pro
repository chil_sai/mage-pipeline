; converts an image from counts to electrons using the information 
; from the fits header and updating the header
; the converted image is returned
; 
; the read-out noise is returned via the "rdnoise" keyword
; the original gain is returned via the "o_gain" keyword

function mage_convert_counts, image, header, o_gain=o_gain, rdnoise=rdnoise

    o_gain=sxpar(header,'EGAIN',count=c_egain)
    if(c_egain ne 1) then o_gain=1.0
    rdnoise=sxpar(header,'ENOISE',count=c_enoise)
    if(c_enoise ne 1) then rdnoise=1.0

    sxaddpar,header,'O_GAIN',o_gain,' original ATOD value cts/e-'
    sxaddpar,header,'EGAIN',1.0
    sxaddpar,header,'GAIN',1.0

    return, image*o_gain
end
