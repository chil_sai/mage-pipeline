pro mage_fits_output_mef,filename,data,wl,hdr,fluxcorr=fluxcorr,fcorr_vec=flux_vec,texp=texp

    hdr_out=hdr
    sxdelpar,hdr_out,'NAXIS2'
    sxdelpar,hdr_out,'NAXIS1'
    hdr_out=hdr_out[3:*]
    mkhdr,h_tst,data[*,*,0],/extend,/image
    hdr_out=[h_tst[0:4],hdr_out]
    sxaddpar,hdr_out,'CTYPE1','AWAV'
    sxaddpar,hdr_out,'CUNIT1','Angstrom'
    sxaddpar,hdr_out,'BUNIT','1e-17 erg/cm^2/s/Angstrom'
    sxaddpar,hdr_out,'CRPIX1',1d
    if(n_elements(texp) ne 1) then texp=sxpar(hdr,'EXPTIME')
    airmass=sxpar(hdr,'AIRMASS')

    if(keyword_set(fluxcorr) and n_elements(flux_vec) ne 1) then $
        flux_vec=mrdfits(getenv('MAGE_PIPELINE_PATH')+'calib_MagE/sensitivity/MagE_flux_corr.fits',1,/silent)
    writefits,filename,0

    n_wl=n_elements(data[*,0,0])
    n_pix=n_elements(data[0,*,0])
    n_ord=n_elements(data[0,0,*])

    sxaddpar,hdr_out,'NAXIS1',n_wl,after='NAXIS'
    if(n_pix gt 1) then begin
        sxaddpar,hdr_out,'NAXIS2',n_pix,after='NAXIS1'
        sxaddpar,hdr_out,'CTYPE2',''
        sxaddpar,hdr_out,'CUNIT2',''
        sxaddpar,hdr_out,'CRPIX2',1.0
        sxaddpar,hdr_out,'CRVAL2',0.0
        sxaddpar,hdr_out,'CDELT2',1.0
        sxaddpar,hdr_out,'CD2_2',1.0
    endif

    for i=0,n_ord-1 do begin
        ordname=(n_ord gt 2)? 'MAGEOR'+string(19-i,format='(i2.2)') : ((i eq 0)? 'FLUX' : 'ERROR')
        sxaddpar,hdr_out,'CRVAL1',wl[0,i]
        sxaddpar,hdr_out,'CDELT1',wl[1,i]-wl[0,i]
        sxaddpar,hdr_out,'CD1_1',wl[1,i]-wl[0,i]
        if(keyword_set(fluxcorr)) then begin
            corrvec=interpol(flux_vec.flux_erg,flux_vec.wave,wl[*,i])/mage_calc_ext_lco(flux_vec.wave,airmass)
            corr_arr=congrid(reform(corrvec,n_wl,1),n_wl,n_pix)/texp*1d17
        endif else corr_arr=1d
        mwrfits,float(data[*,*,i]*corr_arr),filename,hdr_out,/silent
    endfor

end
