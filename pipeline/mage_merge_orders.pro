function mage_merge_orders,spec_extr_2d,flatn_extr_2d,$
    extr_prof_2d,kwl3d,$
    ord_corr,wl_out=wl_m,ord_wl_shift=ord_wl_shift,ord_wl_nm=ord_wl_nm,$
    gain=gain,rdnoise=rdnoise,spec_p_extr_2d=spec_p_extr_2d,$
    ord_all=ord_all,sky_all=sky_all,err_ord_all=err_ord_all,flatn_thr=flatn_thr,flatn_sthr=flatn_sthr,$
    autowl=autowl,sky_merged=sky_merged,specerr_merged=specerr_merged,$
    clean=clean,$
    ord_conv_sigma=ord_conv_sigma,$
    correct_telluric=correct_telluric,$
    telluric_data=telluric_data,$
    spec_2d_merged=spec_2d_merged,$
    specerr_2d_merged=specerr_2d_merged

    wlscl=(keyword_set(ord_wl_nm))? 10d : 1d

    if(keyword_set(autowl)) then $
        wl_m=3300.0d +dindgen(24000)*0.3d

    if(n_elements(gain) ne 1) then gain=1d
    if(n_elements(rdnoise) ne 1) then rdnoise=1d
    if(n_elements(spec_p_extr_2d) ne n_elements(spec_extr_2d)) then spec_p_extr_2d=abs(spec_extr_2d)

    if(n_elements(ord_conv_sigma) ne 1) then ord_conv_sigma=0d
    n_wl_m=n_elements(wl_m)
    s_extr=size(spec_extr_2d)
    ny_ord=s_extr[2]
    n_ord=s_extr[3]

    if(n_elements(flatn_thr) ne 1) then flatn_thr=0.01
    if(n_elements(flatn_sthr) ne 1) then flatn_sthr=1d-4
    illum_corr_ord=spec_extr_2d*0d +1d ;;readfits('calib_FIRE/slits/illumination_orders_e0_45.fits',/silent)

    ord_all=dblarr(n_wl_m,n_ord)
    err_ord_all=dblarr(n_wl_m,n_ord)
    ord_2d_all=dblarr(n_wl_m,ny_ord,n_ord)
    err_ord_2d_all=dblarr(n_wl_m,ny_ord,n_ord)
    sky_all=dblarr(n_wl_m,n_ord)
    wgt_all=dblarr(n_wl_m,n_ord)
    for i=0,n_ord-1 do begin
        print,'Preparing order #'+string(19-i,format='(i3)')
        wl_ord_idx=where(wl_m gt min(ord_corr[i].wl,max=wlmaxcur) and wl_m lt wlmaxcur, cwl_ord_idx)
        if(cwl_ord_idx eq 0) then continue
        wl_ord_cur=wl_m[wl_ord_idx]
        if(n_elements(ord_wl_shift) eq n_ord) then $
            wl_ord_cur-=(n_elements(ord_wl_shift[i].wl_shift) eq 1)? wlscl*ord_wl_shift[i].wl_shift[0] : wlscl*interpol(ord_wl_shift[i].wl_shift,ord_wl_shift[i].wl_coord,wl_m[wl_ord_idx],quadratic=(n_elements(ord_wl_shift[i].wl_shift) eq 3),spline=(n_elements(ord_wl_shift[i].wl_shift) gt 3))
        order_cur=mage_order_linearisation(spec_extr_2d[*,*,i]/illum_corr_ord[*,*,i],i,kwl3d,wl_ord_cur)
        order_p_cur=mage_order_linearisation(spec_p_extr_2d[*,*,i]/illum_corr_ord[*,*,i],i,kwl3d,wl_ord_cur)
        flatn_extr_2d_cur=flatn_extr_2d[*,*,i]
        flatn_extr_2d_cur[0:19,*]=!values.d_nan
        flatn_extr_2d_cur[2028:*,*]=!values.d_nan
        flatn_cur=mage_order_linearisation(flatn_extr_2d_cur,i,kwl3d,wl_ord_cur)
        extr_fit_cur=dblarr(cwl_ord_idx,ny_ord)
        extr_flat_cur=dblarr(cwl_ord_idx,ny_ord)
        extr_flat_cur[*,5:ny_ord-6]=1d/double(ny_ord-10)
        for y=0,ny_ord-1 do extr_fit_cur[*,y]=interpol(extr_prof_2d[*,y,i],ord_corr[i].wl,wl_ord_cur)
        ord_corr_cur=(ord_corr[i].lin_order eq 0)? interpol(ord_corr[i].correction_vector,ord_corr[i].wl,wl_ord_cur) : congrid(ord_corr[i].correction_vector_lin,n_elements(wl_ord_cur),/interp)
        ord_corr_cur_2d=congrid(reform(ord_corr_cur,n_elements(ord_corr_cur),1),n_elements(ord_corr_cur),ny_ord)
        order_cur_var=(((order_p_cur*flatn_cur)>0)*gain+rdnoise^2)/flatn_cur^2/((wl_m[1]-wl_m[0])/(ord_corr[i].wl[1]-ord_corr[i].wl[0]))

        order_cur/=ord_corr_cur_2d
        order_p_cur/=ord_corr_cur_2d
        order_cur_var/=ord_corr_cur_2d
        ord_x_cur=mage_extract_lin_order(order_cur,extr_fit_cur,var2d=order_cur_var,sky=sky_cur,clean=clean,error=err_ord_x_cur)
        tmp_flat_x_cur=mage_extract_lin_order(flatn_cur,extr_flat_cur,sum=flat_x_cur,clean=clean)
        ;;;tmp_p_x_cur=mage_extract_lin_order(order_p_cur*flatn_cur,abs(extr_fit_cur),sum_extr=ord_p_x_cur)
;        ord_x_cur/=ord_corr_cur
;        err_ord_x_cur/=ord_corr_cur
;        sky_cur/=ord_corr_cur
        ;;;err_ord_x_cur=sqrt((ord_p_x_cur>0)*gain+rdnoise^2)/ord_corr_cur/flat_x_cur/gain/sqrt((wl_m[1]-wl_m[0])/(ord_corr[i].wl[1]-ord_corr[i].wl[0]))
        if(ord_conv_sigma gt 0) then begin
            ord_x_cur=mage_convol_order(wl_m[wl_ord_idx],ord_x_cur,ord_conv_sigma)
            sky_cur=mage_convol_order(wl_m[wl_ord_idx],sky_cur,ord_conv_sigma)
            err_ord_x_cur=mage_convol_order(wl_m[wl_ord_idx],err_ord_x_cur,ord_conv_sigma)
            order_cur=mage_convol_order(wl_m[wl_ord_idx],order_cur,ord_conv_sigma)
            order_cur_var=mage_convol_order(wl_m[wl_ord_idx],order_cur_var,ord_conv_sigma)
        endif
        if(keyword_set(correct_telluric) and n_elements(telluric_data) eq n_ord) then begin
            telluric_order=interpol(telluric_data[i].transmission,telluric_data[i].wl,wl_m[wl_ord_idx]) ;,/spl)
            ord_x_cur/=telluric_order
            err_ord_x_cur/=telluric_order
            order_cur/=congrid(reform(telluric_order,n_elements(telluric_order),1),n_elements(telluric_order),ny_ord)
            order_cur_var/=congrid(reform(telluric_order,n_elements(telluric_order),1),n_elements(telluric_order),ny_ord)
        endif
        ord_all[wl_ord_idx,i]=ord_x_cur
        err_ord_all[wl_ord_idx,i]=err_ord_x_cur
        sky_all[wl_ord_idx,i]=sky_cur
        ord_2d_all[wl_ord_idx,*,i]=order_cur
        err_ord_2d_all[wl_ord_idx,*,i]=sqrt(order_cur_var)
        wgt_all[wl_ord_idx,i]=1d
        max_fl_x_cur=max(flat_x_cur,/nan)
        bflat=where((flat_x_cur le flatn_thr) or $
                    ((flat_x_cur le flatn_sthr) and $
                     (flat_x_cur le 0.05*max_fl_x_cur)) or $
                    (finite(ord_corr_cur) ne 1),cbflat)
;print,'maxfl=',max_fl_x_cur,cbflat
        if(cbflat gt 0) then wgt_all[wl_ord_idx[bflat],i]=0d
        blue_end_overlap_idx=where((finite(ord_corr_cur) eq 1) and $
                                   (flat_x_cur gt flatn_thr) and $
                                   ((flat_x_cur gt flatn_sthr) or (flat_x_cur gt 0.05*max_fl_x_cur)) and $
                                   (wl_ord_cur le ord_corr[i].wl[ord_corr[i].overlap_end]),cblue_end_overlap_idx)
        if(i eq 0) then cblue_end_overlap_idx=0
        if(cblue_end_overlap_idx gt 1) then begin
            wgt_all[wl_ord_idx[blue_end_overlap_idx],i]=dindgen(cblue_end_overlap_idx)/double(cblue_end_overlap_idx-1)*flat_x_cur[blue_end_overlap_idx]
        endif
        red_end_overlap_idx= where((finite(ord_corr_cur) eq 1) and $
                                   (flat_x_cur gt flatn_thr) and $
                                   ((flat_x_cur gt flatn_sthr) or (flat_x_cur gt 0.05*max_fl_x_cur)) and $
                                   (wl_ord_cur gt ord_corr[i].wl[ord_corr[i].overlap_start]),cred_end_overlap_idx)
        if(i eq n_ord-1) then cred_end_overlap_idx=0
        if(cred_end_overlap_idx gt 1) then begin
            wgt_all[wl_ord_idx[red_end_overlap_idx],i]=(1d -dindgen(cred_end_overlap_idx)/double(cred_end_overlap_idx-1))*flat_x_cur[red_end_overlap_idx]
        endif
    endfor

    spec_merged=total(ord_all*wgt_all,2,/nan)/total(wgt_all,2)
    specerr_merged=sqrt(total((err_ord_all^2)*wgt_all,2,/nan)/total(wgt_all,2))
    sky_merged=total(sky_all*wgt_all,2,/nan)/total(wgt_all,2)

    wgt_2d_all=congrid(reform(wgt_all,n_wl_m,1,n_ord),n_wl_m,ny_ord,n_ord)
    bad_pix_2d=where(finite(ord_2d_all) ne 1, cbad_pix_2d)
    if(cbad_pix_2d gt 0) then wgt_2d_all[bad_pix_2d]=0d
    spec_2d_merged=total(ord_2d_all*wgt_2d_all,3,/nan)/total(wgt_2d_all,3)
    specerr_2d_merged=sqrt(total((err_ord_2d_all^2)*wgt_2d_all,3,/nan)/total(wgt_2d_all,3))

    return,spec_merged
end
