function remove_overscan,image,datasec,subtract=subtract,median=median,direction=direction
;;; subtracts overscan given an image and a datasec array (4 numbers)
    s_im=size(image)
    if(n_elements(direction) ne 1) then direction=0 ;;; X

    overscan_x=[-1]
    if(datasec[0] gt 1) then overscan_x=[overscan_x,lindgen(datasec[0]-1l)]
    overscan_y=[-1]
    if(datasec[2] gt 1) then overscan_y=[overscan_y,lindgen(datasec[2]-1l)]
    if(datasec[1] lt s_im[1]) then $
        overscan_x=[overscan_x,datasec[1]+lindgen(s_im[1]-datasec[1]-1l)]
    if(datasec[3] lt s_im[2]) then $
        overscan_y=[overscan_y,datasec[3]+lindgen(s_im[2]-datasec[3]-1l)]
    image_sub=image
    if(n_elements(overscan_x) gt 1 and keyword_set(subtract) and direction eq 0) then begin
        overscan_x=overscan_x[1:*]
        if(keyword_set(median)) then $
            overscan_vec=median(image_sub[overscan_x,*],dim=1) else $
            resistant_mean,image_sub[overscan_x,*],3.0,overscan_vec,dim=1
        image_sub=image_sub-((fltarr(s_im[1])+1) # overscan_vec)
    endif
    if(n_elements(overscan_y) gt 1 and keyword_set(subtract) and direction ne 0) then begin
        overscan_y=overscan_y[1:*]
        if(keyword_set(median)) then $
            overscan_vec=median(image_sub[*,overscan_y],dim=2) else $
            resistant_mean,image_sub[*,overscan_y],3.0,overscan_vec,dim=2
        image_sub=image_sub-(overscan_vec # (fltarr(s_im[2])+1))
    endif
    img_sub=image_sub[datasec[0]-1:datasec[1]-1,datasec[2]-1:datasec[3]-1]
    return,img_sub
end

function parse_secstr,secstr
;;; parses fits keywords in the format: '[1:8192,1:4112]' into lonarr(4)
    return,long(strsplit(strmid(secstr,1,strlen(secstr)-2),'([:,])',/regex,/extract))
end

function mage_subtract_overscan,image,header,$
        satlevel=satlevel,median=median,satmask=satmask
    if(n_elements(satlevel) ne 1) then satlevel=65530.
    if(n_params() eq 1) then begin
        biassec=[2049,2176,1025,1152]
    endif else begin
        biassec=parse_secstr(sxpar(header,'BIASSEC'))
    endelse
    datasec=[1,biassec[0]-1,1,biassec[2]-1]

    image_cur=double(image)
    satidx=where(image_cur ge satlevel, csatidx)
    if(csatidx gt 0 and keyword_set(satmask)) then $
        image_cur[satidx]=!values.d_nan

    image_new=remove_overscan(image_cur,datasec,/sub,median=median)
    if(n_elements(image_new[0,*]) eq 1028) then image_new=image_new[*,0:1023]

    return,image_new
end
