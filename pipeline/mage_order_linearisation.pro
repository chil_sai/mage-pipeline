function mage_order_linearisation,order_img,n,kwl3d,wl,truen=truen,autowl=autowl,wlmap=wl_cur
    s_order=size(order_img)
    x_ord=reform(dindgen(s_order[1]) # (dblarr(s_order[2])+1d), s_order[1]*s_order[2])
    l_ord=reform((dblarr(s_order[1])+1d) # dindgen(s_order[2])/36d, s_order[1]*s_order[2])

    n_cur = (keyword_set(truen))? n : 19-n
    wl_cur=reform(poly3d(x_ord*0d +double(n_cur),x_ord,l_ord,kwl3d,/irreg),s_order[1],s_order[2])
    if((n_params() eq 3) or keyword_set(autowl)) then $
        wl=min(wl_cur[*,18])+abs(wl_cur[s_order[1]/2,18]-wl_cur[s_order[1]/2-1,18])*dindgen(s_order[1])

    ord_lin=dblarr(n_elements(wl),s_order[2])
    for j=0,s_order[2]-1 do ord_lin[*,j]=interpol(order_img[*,j],wl_cur[*,j],wl)

    return,ord_lin
end

