function mage_order_to_image,order_img,trace_coeff,n_ord,x0=x0,nx=nx,ny=ny

    s_ord=size(order_img)
    ny_order=s_ord[2]
    if(n_elements(nx) ne 1) then nx=2048
    if(n_elements(ny) ne 1) then ny=1024
    if(n_elements(x0) ne 1) then x0=nx/2

    im=fltarr(nx,ny)

    x_vec=findgen(nx)-x0
    order_low = poly(x_vec,trace_coeff[*,n_ord,0])
    order_hi = poly(x_vec,trace_coeff[*,n_ord,1])

    for x=0,nx-1 do begin
        if(fix(order_low[x]) lt 0 or fix(order_hi[x]) gt ny-1) then continue
        xx=order_low[x]+dindgen(ny_order)*(order_hi[x]-order_low[x])/(ny_order-1.)
        im_col=interpol(order_img[x,*],$
                        order_low[x]+dindgen(ny_order)*(order_hi[x]-order_low[x])/(ny_order-1.),$
                        fix(order_low[x])+dindgen(fix(order_hi[x])-fix(order_low[x])+1))
        im[x,fix(order_low[x]):fix(order_hi[x])]=transpose(im_col)
    endfor

    return, im
end
