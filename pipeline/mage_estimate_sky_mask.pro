;; primitive estimate of the sky background region for a point source

function mage_estimate_sky_mask,image,trace_coeff,order=order,nsky=nsky

    if(n_elements(order) ne 1) then order=6

    ord_cur=mage_extract_order(image,trace_coeff,order)
    s_ord=size(ord_cur)
    nx=s_ord[1]
    ny=s_ord[2]
    
    if(n_elements(nsky) ne 1) then nsky=ny
    sky_mask_full=intarr(ny)
    sky_mask=intarr(nsky)

    prof_slit=median(ord_cur[nx*0.3:nx*0.7,*],dim=1)
    prof_mask=intarr(ny)+1
    prof_mask[0:2]=0
    prof_mask[ny-3:ny-1]=0

    good_prof=where(finite(prof_slit) eq 1 and prof_mask eq 1, cgood_prof, compl=bad_prof, ncompl=cbad_prof)
    if(cgood_prof lt 10) then return,0 else prof_mask[good_prof]=1
    if(cbad_prof gt 0) then prof_mask[bad_prof]=0

    good_idx=where(prof_mask eq 1)
    g = mpfitpeak(double(good_idx),prof_slit[good_idx],k,nterms=4)

    sky_mask_full[good_idx]=1
    if(k[2] gt 0.5 and k[2] lt 7) then begin ;; Gaussian sigma between 0.2 and 2.3 arcsec
        gpsf=psf_gaussian(ndim=1,npix=ny,cent=k[1],fwhm=2.355*k[2])
        psf_bright=where(gpsf gt 0.01, cpsf_bright)
        if(cpsf_bright gt 0) then sky_mask_full[psf_bright]=0
    endif else return,0
    sky_mask=(ny ne nsky)? congrid(sky_mask_full,nsky) : sky_mask_full

    return,sky_mask
end
