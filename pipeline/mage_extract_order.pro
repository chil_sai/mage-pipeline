function mage_extract_order,im,trace_coeff,n_ord,x0=x0,ny=ny,ymap=ymap

    s_im=size(im)
    if(n_elements(x0) ne 1) then x0=s_im[1]/2

    if(n_elements(ny) ne 1) then ny=37
    order_img=fltarr(s_im[1],ny)+!values.f_nan
    ymap=fltarr(s_im[1],ny)+!values.f_nan

    x_vec=findgen(s_im[1])-x0
    order_low = poly(x_vec,trace_coeff[*,n_ord,0])
    order_hi = poly(x_vec,trace_coeff[*,n_ord,1])

    for x=0,s_im[1]-1 do begin
        if(order_low[x] lt 0 or order_hi[x] gt s_im[2]-1) then continue
        xx=order_low[x]+dindgen(ny)*(order_hi[x]-order_low[x])/(ny-1.)
        order_img[x,*]=interpolate(transpose(im[x,*]),xx)
        ymap[x,*]=xx
    endfor

    return, order_img
end
