pro mage_display_arc_lines,image,fit_data,kwl3d,line_data,wl_lines,good_id_max,$
    zoom=zoom,dy_vis=dy_vis,flmin=flmin,flmax=flmax,noresid=noresid,$
    verbose=verbose,d_lam_thr=d_lam_thr

    if(n_elements(flmin) ne 1) then flmin=-100.
    if(n_elements(flmax) ne 1) then flmax=6e3
    if(n_elements(zoom) ne 1) then zoom=40.
    if(n_elements(dy_vis) ne 1) then dy_vis=0
    if(n_elements(d_lam_thr) ne 1) then d_lam_thr=0.3 ;;; 0.3A

    loadct,0
    tv,bytscl(image[*,dy_vis:*],flmin,flmax)
    loadct,39
    cgood_id_max=n_elements(good_id_max)
    lam_fit=reform(poly3d(transpose(fit_data[0,*]),transpose(fit_data[1,*]),transpose(fit_data[2,*]),kwl3d,/irreg),37l,cgood_id_max)
    for i=0,n_elements(line_data)-1 do plots,line_data[i].xpos,line_data[i].ypos-dy_vis,col=128,/dev; psym=7,syms=0.2
    for k=0,cgood_id_max-1 do if(finite(wl_lines[good_id_max[k]]) eq 1) then begin
        if(~keyword_set(noresid)) then $
            for j=0,36 do plots,[0,(wl_lines[good_id_max[k]]-lam_fit[j,k])*zoom]+line_data[good_id_max[k]].xpos[j],[0d,0d]+line_data[good_id_max[k]].ypos[j]-dy_vis,$
                                col=((wl_lines[good_id_max[k]] gt lam_fit[j,k])? 254 : 60),psym=-3,/dev
        arrow,median(line_data[good_id_max[k]].xpos),median(line_data[good_id_max[k]].ypos)-dy_vis,$
              median(line_data[good_id_max[k]].xpos)+median(wl_lines[good_id_max[k]]-lam_fit[*,k])*zoom,median(line_data[good_id_max[k]].ypos)-dy_vis,col=192,hsize=5
        if(keyword_set(verbose)) then begin
            d_lam_cur=median(wl_lines[good_id_max[k]]-lam_fit[*,k])
            if(abs(d_lam_cur) gt d_lam_thr) then $
                print,'High offset line (>',d_lam_thr,'A), Ord #',line_data[good_id_max[k]].order,' wl=',wl_lines[good_id_max[k]],' dwl=',d_lam_cur,'A',$
                    format='(a,f4.2,a,i2,a,f8.2,a,f5.2,a)'
        endif
    endif

end
