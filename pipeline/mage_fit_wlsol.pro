function mage_wlsol_filter_good_lines,fit_data,err_data,kwl3d,wl_lines,good_id_max,$
    good_idx=good_idx,dlam_thr=dlam_thr,rms_thr=rms_thr,min_rms=min_rms,cres_idx=cres_idx,pixels=pixels

    cgood_id_max=n_elements(good_id_max)
    n_pnt=n_elements(fit_data[0,*])
    if(n_elements(good_idx) le 1 or n_elements(good_idx) gt n_pnt) then good_idx=lindgen(n_pnt)
    data_mask=bytarr(37l,cgood_id_max)
    data_mask[good_idx]=1

    lam_fit=reform(poly3d(transpose(fit_data[0,*]),transpose(fit_data[1,*]),transpose(fit_data[2,*]),kwl3d,/irreg),37l,cgood_id_max)
    lam_off_fit=reform(poly3d(transpose(fit_data[0,*]),transpose(fit_data[1,*])+1.0,transpose(fit_data[2,*]),kwl3d,/irreg),37l,cgood_id_max)
    disp_fit=abs(lam_off_fit-lam_fit)
    dlam_fit=lam_fit*!values.d_nan
    med_dlam=lam_fit*!values.d_nan
    med_disp=lam_fit*!values.d_nan
    std_dlam=lam_fit*!values.d_nan

    min_g_frac=0.75 ;;; 25% of pixels along the slit may be missing
    for k=0,cgood_id_max-1 do if(finite(wl_lines[good_id_max[k]]) eq 1) then begin
          dlam_fit[*,k]=wl_lines[good_id_max[k]]-lam_fit[*,k]
          good_pix_line=where(finite(dlam_fit[*,k]) eq 1 and data_mask[*,k] eq 1, cgood_pix_line)
          if(cgood_pix_line gt 37*min_g_frac) then begin
              med_dlam[*,k]=median(dlam_fit[good_pix_line,k])
              std_dlam[*,k]=stddev(dlam_fit[good_pix_line,k])
              med_dlam[*,k]=median(disp_fit[good_pix_line,k])
          endif
    endif

    res_idx=(keyword_set(pixels))? $
        where((data_mask eq 1) and $
                   ((med_dlam le dlam_thr and std_dlam le rms_thr) or (err_data lt min_rms)),cres_idx) : $
        where((data_mask eq 1) and $
                   ((med_dlam/med_disp le dlam_thr and std_dlam le rms_thr) or (err_data lt min_rms)),cres_idx)

    return,res_idx
end


function mage_fit_wlsol,arc_thar,flat,oh_frame=oh_frame,use_oh=use_oh,$
    deg_fit=deg_fit,clay=clay,domeflat=domeflat,$
    trace_coeff=trace_coeff,display=display,dy_vis=dy_vis,detect_fwhm=detect_fwhm,$
    disp_rel_qc=disp_rel_qc

    if(n_elements(dy_vis) ne 1) then dy_vis=0
    if(n_elements(deg_fit) ne 3) then deg_fit=[4,7,1]
    if(n_elements(detect_fwhm) ne 1) then detect_fwhm=3.0

    slith=37l
    pixflat=1d ;readfits('calib_FIRE/detector/H2RG/pixflat.fits',/silent)
    s_arc=size(arc_thar)
    s_oh=size(oh_frame)
    if(s_oh[1] ne s_arc[1] or s_oh[2] ne s_arc[2]) then use_oh=0

    ini_fnsuff=(keyword_set(clay))? '.clay' : ''
    wl_ini=mrdfits(getenv('MAGE_PIPELINE_PATH')+'calib_MagE/wavelength/mage_wl_ini'+ini_fnsuff+'.fits',1,/silent)
    for wl_iter=0,0 do begin
        trace_coeff=mage_flat_trace(flat*1e+4,x_vec=x_vec,o_arr=o_arr,clay=clay,domeflat=domeflat); ,n_poly=[5,9])
        n_ord=(size(o_arr))[1]

        mask=bytarr(24,24)
        mask[*,23]=1
        mask[0:3,21:22]=1
        mask[21:*,22]=1
        mask[0:16,0]=1
        mask[0:10,0]=1
        blaze=compute_binned_flat(flat,nx=24,ny=24,/fit,deg_fit=[4,5],mask=mask)
        flat_norm=congrid(blaze/max(blaze,/nan),2048,1024,/interp)

        if(wl_iter eq 0) then begin
            t=mage_detect_arc_lines(median(arc_thar/flat_norm,3),trace_coeff,detect_fwhm,maxrms=1.5,maxvisflux=2500.0,smy=3,dy_vis=dy_vis,ndeg_y=1,display=display) ;;; fwhm=4,maxrms=2.5
        endif else begin
            for lidx=0,n_elements(t)-1 do begin
                t[lidx].xpos=t[lidx].xpos+poly(t[lidx].xpos,xdtr)
                t[lidx].ypos=t[lidx].ypos+poly(t[lidx].ypos,ydtr)
                t[lidx].xfit=t[lidx].xfit+poly(t[lidx].xfit,xdtr)
                t[lidx].yfit=t[lidx].yfit+poly(t[lidx].yfit,ydtr)
                t[lidx].xmean=t[lidx].xmean+poly(t[lidx].xmean,xdtr)
            endfor
            wl_ini=mage_get_wl_ini_from_data(t,lin_iter3,kwl3d,deg1=deg_fit[0],deg2=deg_fit[1],/mean)
        endelse

        zoom=40.0 ;; 40.0
        min_rms_thr=0.0 ; 0.7 ;;; always accept strong lines
        dwl=0.5
        for fit_iter=0,4-4*wl_iter do begin
            wl_lines=mage_ident_arc_2d(t,minflux=80.0/(1.0+fit_iter*0.5),wl_ini=wl_ini,$
                fwhm=4.0/(0.7+fit_iter/4.0),nwl_step=(fit_iter eq 0)? 150 : ((40-20*(fit_iter-1))>0))
            disp_lines=abs($
                poly2d(double(t.order),double(t.xmean),wl_ini.kx,deg1=wl_ini.deg1,deg2=wl_ini.deg2,/irreg)-$
                poly2d(double(t.order),double(t.xmean)+1.0,wl_ini.kx,deg1=wl_ini.deg1,deg2=wl_ini.deg2,/irreg))
            good_id_max=where(wl_lines gt 100.0 and finite(wl_lines) eq 1 and t.rms lt 1.2, cgood_id_max)
            print,'lines identified: ',cgood_id_max
            fit_data=dblarr(4,slith*cgood_id_max)
            linid_data=reform((lonarr(slith)+1l) # good_id_max,slith*cgood_id_max)
            fit_data[0,*]=reform((lonarr(slith)+1l) # t[good_id_max].order,slith*cgood_id_max)
            fit_data[1,*]=reform(t[good_id_max].xpos,slith*cgood_id_max)
            fit_data[2,*]=reform(dindgen(slith)/double(slith-1) # (lonarr(cgood_id_max)+1l),slith*cgood_id_max)
            fit_data[3,*]=reform((lonarr(slith)+1l) # wl_lines[good_id_max],slith*cgood_id_max)
            err_data=reform((lonarr(slith)+1l) # (t[good_id_max].rms*disp_lines[good_id_max]),slith*cgood_id_max)^1.0
            err_data_fit=err_data ;;;; /(1.0+2d*(((fit_data[1,*]-500d)/1024d)>0)^2)
;            top_frame=where(fit_data[1,*] gt 500d)
;            err_data_fit[top_frame]=err_data_fit[top_frame]/(1.0+2.0*(((fit_data[0,top_frame]-1024d)/1024d)>0)^2)
            g_data=where(finite(total(fit_data,1)) eq 1,cg_data)

            ttt=vfit_3deg_err(fit_data[*,g_data],err=err_data_fit[g_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg,/max)
    ;;        plot,fit_data[1,g_data],fit_data[3,g_data]-ttt,xs=1,ys=1,psym=4
            print,'RMS(wlsol)[A] =',robust_sigma(fit_data[3,g_data]-ttt)
            if (keyword_set(display)) then mage_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,zoom=zoom

            ;;;iter1_data=g_data[where((abs(fit_data[3,g_data]-ttt) le 3.*(err_data[g_data])*dwl*22d/fit_data[0,g_data]) or (fit_data[0,g_data] le 10*0) or (err_data[g_data] lt min_rms_thr), citer1_data)] ;; 0.1
            iter1_data=mage_wlsol_filter_good_lines(fit_data,err_data,kwl3d,wl_lines,good_id_max,$
                good_idx=g_data,dlam_thr=dwl*20.0/(1.0+0.4*fit_iter),rms_thr=dwl*3.0,min_rms=min_rms_thr,cres_idx=citer1_data,/pix)
            ttt1=vfit_3deg_err(fit_data[*,iter1_data],err=err_data_fit[iter1_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg)
    ;;        plot,fit_data[1,iter1_data],fit_data[3,iter1_data]-ttt1,xs=1,ys=1,psym=4
            print,'RMS(wlsol)[A] =',robust_sigma(fit_data[3,iter1_data]-ttt1)
            if (keyword_set(display)) then mage_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,zoom=zoom

            min_rms_thr_final=min_rms_thr/(0.2*fit_iter+1)


            iter2_data=mage_wlsol_filter_good_lines(fit_data,err_data,kwl3d,wl_lines,good_id_max,$
                good_idx=iter1_data,dlam_thr=dwl*5.0/(1.0+0.2*fit_iter),rms_thr=dwl*2.0,min_rms=min_rms_thr_final,cres_idx=citer2_data,/pix)
            ttt2=vfit_3deg_err(fit_data[*,iter2_data],err=err_data_fit[iter2_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg)
    ;;        plot,fit_data[1,iter2_data],fit_data[3,iter2_data]-ttt2,xs=1,ys=1,psym=4
            print,'RMS(wlsol)[A] =',robust_sigma(fit_data[3,iter2_data]-ttt2)
            if (keyword_set(display)) then mage_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,zoom=zoom

            lin_iter2=linid_data[iter2_data[uniq(linid_data[iter2_data],sort(linid_data[iter2_data]))]]
            wl_ini=mage_get_wl_ini_from_data(t,lin_iter2,kwl3d,deg1=deg_fit[0],deg2=deg_fit[1],/mean)
        endfor


        iter3_data=mage_wlsol_filter_good_lines(fit_data,err_data,kwl3d,wl_lines,good_id_max,$
            good_idx=iter2_data,dlam_thr=dwl*2.0,rms_thr=dwl,min_rms=min_rms_thr_final*0.0,cres_idx=citer3_data,/pix) ;;; was dlam_thr=dwl*1.5
        ttt3=vfit_3deg_err(fit_data[*,iter3_data],err=err_data_fit[iter3_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg)
;;        plot,fit_data[1,iter3_data],fit_data[3,iter3_data]-ttt3,xs=1,ys=1,psym=4
        print,'RMS(wlsol)[A] =',robust_sigma(fit_data[3,iter3_data]-ttt3),'; N of data points: ',citer3_data

        lin_iter3=linid_data[iter3_data[uniq(linid_data[iter3_data],sort(linid_data[iter3_data]))]]
        wl_ini=mage_get_wl_ini_from_data(t,lin_iter3,kwl3d,deg1=deg_fit[0],deg2=deg_fit[1],/mean)

        if (keyword_set(display)) then mage_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,zoom=zoom,/verb,d_lam_thr=0.5;,/noresid
        data_used=mage_xnl2y(transpose(fit_data[1,*]),$
                             transpose(fit_data[0,*]),$
                             transpose(fit_data[2,*]),trace_coeff,/truen)
        if(keyword_set(display)) then begin
            plots,fit_data[1,iter3_data],data_used[iter3_data]-dy_vis,/dev,col=220,psym=4,syms=0.3
        endif
        ord_fit=fit_data[0,iter3_data[uniq(fit_data[0,iter3_data],sort(fit_data[0,iter3_data]))]]
        n_ord_fit=n_elements(ord_fit)
        disp_rel_qc=replicate({order:-1l,xmin:-1l,xmax:-1l,n_lines:-1l,rms_nm:!values.f_nan,rms_kms:!values.f_nan,dv_kms:!values.f_nan},n_ord_fit)
        for oo=0,n_ord_fit-1 do begin
            ord_cur_idx=where(fit_data[0,iter3_data] eq ord_fit[oo])
            ord_data_idx=iter3_data[ord_cur_idx]
            ord_cur_line_ids=linid_data[uniq(linid_data[ord_data_idx],sort(linid_data[ord_data_idx]))]
            ord_cur_min_x=min(fit_data[1,ord_data_idx],max=ord_cur_max_x)
            disp_rel_qc[oo].order=ord_fit[oo]
            disp_rel_qc[oo].xmin=ord_cur_min_x
            disp_rel_qc[oo].xmax=ord_cur_max_x
            disp_rel_qc[oo].n_lines=n_elements(ord_cur_line_ids)
            disp_rel_qc[oo].rms_nm=robust_sigma(fit_data[3,ord_data_idx]-ttt3[ord_cur_idx])/10d
            disp_rel_qc[oo].rms_kms=299792.5*robust_sigma((fit_data[3,ord_data_idx]-ttt3[ord_cur_idx])/fit_data[3,ord_data_idx])
            disp_rel_qc[oo].dv_kms=299792.5*median((fit_data[3,ord_data_idx]-ttt3[ord_cur_idx])/fit_data[3,ord_data_idx])
            print,'Ord#',disp_rel_qc[oo].order,' xrange=',disp_rel_qc[oo].xmin,disp_rel_qc[oo].xmax,$
                ' nlin=',disp_rel_qc[oo].n_lines,$
                ' RMS=',disp_rel_qc[oo].rms_nm*10d,'A/',disp_rel_qc[oo].rms_kms,'km/s',$
                ' <dv>=',disp_rel_qc[oo].dv_kms,'km/s',$
                format='(a,i2,a,i4,1x,i4,a,i3,a,f4.2,a,f5.1,a,a,f5.1,a)'
        endfor
        print,'Total number of lines used:',total(disp_rel_qc.n_lines)

        if((wl_iter eq 1) and keyword_set(use_oh)) then begin
            wl_ini_oh=wl_ini
            for oh_fit_iter=0,2 do begin
                wl_lines_oh=mage_ident_arc_2d(toh,wl_ini=wl_ini_oh,fwhm=3.5-1.0*(oh_fit_iter ge 1),minfl=0.2d,nwl_step=(oh_fit_iter eq 0? 30 : 0),/oh)
                nn=12
                wp=where(toh.order eq nn,cwp)
                sl=(sort(toh[wp].xmean))
                for i=0,cwp-1 do print,toh[wp[sl[i]]].xmean,toh[wp[sl[i]]].flux[((slith-1)/2)],toh[wp[sl[i]]].rms,toh[wp[sl[i]]].wavelength

                good_id_max_oh=where(wl_lines_oh gt 100.0 and finite(wl_lines_oh) eq 1 and $
                                    ((toh.rms lt 1.0) or ((toh.xmean lt 500) and (toh.ypos[((slith-1)/2)] gt 1500))) and (toh.order lt 20), cgood_id_max_oh)
                print,'Number of OH lines identified in orders <19:',string(cgood_id_max_oh,format='(i4)')
                fit_data_oh=dblarr(4,slith*cgood_id_max_oh)
                linid_data_oh=reform((lonarr(slith)+1l) # good_id_max_oh,slith*cgood_id_max_oh)
                fit_data_oh[0,*]=reform((lonarr(slith)+1l) # toh[good_id_max_oh].order,slith*cgood_id_max_oh)
                fit_data_oh[1,*]=reform(toh[good_id_max_oh].xpos,slith*cgood_id_max_oh)
                fit_data_oh[2,*]=reform(dindgen(slith)/double(slith-1) # (lonarr(cgood_id_max_oh)+1l),slith*cgood_id_max_oh)
                fit_data_oh[3,*]=reform((lonarr(slith)+1l) # wl_lines_oh[good_id_max_oh],slith*cgood_id_max_oh)
                err_data_oh=reform((lonarr(slith)+1l) # toh[good_id_max].rms,slith*cgood_id_max)^1.0
                err_data_oh_fit=err_data_oh/(1.0+2d*(((fit_data_oh[1,*]-1500d)/1024d)>0)^2)
                ;top_frame_oh=where(fit_data_oh[1,*] gt 1500d)
                ;err_data_oh_fit[top_frame_oh]=err_data_oh_fit[top_frame_oh]/(1.0+2.0*(((fit_data_oh[0,top_frame_oh]-1024d)/1024d)>0)^2)
                g_data_oh=where(finite(total(fit_data_oh,1)) eq 1,cg_data_oh)

                wl_lines=mage_ident_arc_2d(t,minflux=100.0/(1.0+oh_fit_iter),wl_ini=wl_ini_oh,fwhm=3.5-1.0*(oh_fit_iter ge 1),nwl_step=(oh_fit_iter eq 0? 30 : 0))
                good_id_max=where(wl_lines gt 100.0 and finite(wl_lines) eq 1 and $
                                 ((t.rms lt 1.2) or ((t.rms lt 2.0) and (t.xmean lt 680) and (t.ypos[((slith-1)/2)] gt 1500)) or (t.order le 14)), cgood_id_max)
                print,'ThAr lines identified: ',cgood_id_max
                fit_data=dblarr(4,slith*cgood_id_max)
                linid_data=reform((lonarr(slith)+1l) # good_id_max,slith*cgood_id_max) + n_elements(toh)
                fit_data[0,*]=reform((lonarr(slith)+1l) # t[good_id_max].order,slith*cgood_id_max)
                fit_data[1,*]=reform(t[good_id_max].xpos,slith*cgood_id_max)
                fit_data[2,*]=reform(dindgen(slith)/double(slith-1) # (lonarr(cgood_id_max)+1l),slith*cgood_id_max)
                fit_data[3,*]=reform((lonarr(slith)+1l) # wl_lines[good_id_max],slith*cgood_id_max)
                err_data=reform((lonarr(slith)+1l) # t[good_id_max].rms,slith*cgood_id_max)^1.0
                err_data_fit=err_data/(1.0+2d*(((fit_data[1,*]-500d)/1024d)>0)^2)
                g_data=where(finite(total(fit_data,1)) eq 1,cg_data)

                fit_data_comb=transpose([transpose(fit_data_oh[*,g_data_oh]),transpose(fit_data[*,g_data])])
                err_data_comb=[err_data_oh_fit[g_data_oh],err_data_fit[g_data]]
                linid_data_comb=[linid_data_oh[g_data_oh],linid_data[g_data]]
                tttoh=vfit_3deg_err(fit_data_comb[*,*],err=err_data_comb[*],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,/irreg)
                ;;if(keyword_set(display)) then plot,fit_data_comb[0,*]+fit_data_comb[1,*]/2400.,fit_data_comb[3,*]-tttoh,xs=3,ys=1,psym=4,yr=[-0.3,0.3]
                print,'RMS(wlsol)[A] =',robust_sigma(fit_data_comb[3,*]-tttoh)

                allcomb=lindgen(n_elements(err_data_comb))
                iter1_datacomb=allcomb[where((abs(fit_data_comb[3,allcomb]-tttoh) le $
                    3.*(err_data_comb[allcomb]>0.3)*dwl*22d/fit_data_comb[0,allcomb])  or fit_data_comb[0,allcomb] le 13, citer1_datacomb)] 
                tttoh1=vfit_3deg_err(fit_data_comb[*,iter1_datacomb],err=err_data_comb[iter1_datacomb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,/irreg)
                print,'RMS(wlsol)[A] =',robust_sigma(fit_data_comb[3,iter1_datacomb]-tttoh1)

                iter2_datacomb=iter1_datacomb[where((abs(fit_data_comb[3,iter1_datacomb]-tttoh1) le $
                    1.*(err_data_comb[iter1_datacomb]>0.3)*dwl*22d/fit_data_comb[0,iter1_datacomb]) or fit_data_comb[0,iter1_datacomb] le 13, citer2_datacomb)] 
                tttoh2=vfit_3deg_err(fit_data_comb[*,iter2_datacomb],err=err_data_comb[iter2_datacomb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,/irreg)
                print,'RMS(wlsol)[A] =',robust_sigma(fit_data_comb[3,iter2_datacomb]-tttoh2)

                iter3_datacomb=iter2_datacomb[where((abs(fit_data_comb[3,iter2_datacomb]-tttoh2) le $
                    1.*(err_data_comb[iter2_datacomb]>0.3)*dwl*22d/fit_data_comb[0,iter2_datacomb]), citer3_datacomb)] 
                tttoh3=vfit_3deg_err(fit_data_comb[*,iter3_datacomb],err=err_data_comb[iter3_datacomb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,/irreg)
                print,'RMS(wlsol)[A] =',robust_sigma(fit_data_comb[3,iter3_datacomb]-tttoh3)
    ;            plot,fit_data_comb[0,iter3_datacomb]+fit_data_comb[1,iter3_datacomb]/2400.,fit_data_comb[3,iter3_datacomb]-tttoh3,xs=3,ys=1,psym=4,yr=[-0.3,0.3]

                if (keyword_set(display)) then mage_display_arc_lines,(oh_frame)/flat_norm,fit_data_oh,kwl3d_oh,toh,wl_lines_oh,good_id_max_oh,flmax=300,dy_vis=dy_vis,zoom=zoom
                lin_iter_comb=linid_data_comb[iter3_datacomb[uniq(linid_data_comb[iter3_datacomb],sort(linid_data_comb[iter3_datacomb]))]]
                wl_ini_oh=mage_get_wl_ini_from_data([toh,t],lin_iter_comb,kwl3d_oh,deg1=deg_fit[0],deg2=deg_fit[1],/mean)
            endfor
            kwl3d=kwl3d_oh
            data_used_comb=mage_xnl2y(transpose(fit_data_comb[1,*]),$
                                 transpose(fit_data_comb[0,*]),$
                                 transpose(fit_data_comb[2,*]),trace_coeff,/truen)
            if(keyword_set(display)) then plots,fit_data_comb[1,iter3_datacomb],data_used_comb[iter3_datacomb]-dy_vis,/dev,col=192,psym=4,syms=0.3
        endif
    endfor

    return,kwl3d
end
