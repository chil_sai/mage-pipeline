;; merging neighboor points (closer than fwhm/2)
function filter_1d,x,fwhm, k=k, outnum=outnum, verbose=verbose
if (n_elements(k) ne 1) then k=0.5d
nx=n_elements(x)
flags=intarr(nx)
newx=dblarr(nx)
nnx=0
outnum=intarr(nx,nx)
for i=0,nx-1 do begin
  neighb=where((abs(x-x[i]) le k*fwhm) and (flags eq 0))
  if(total(neighb) ge 0) then begin
    xmed=median(x[neighb])
    flags[neighb]=1
    newx[nnx]=xmed
    outnum[nnx,0:n_elements(neighb)-1]=neighb
    nnx=nnx+1
  endif
endfor

;outnum=outnum[0:nnx-1,0:nnx-1]
outnum=outnum[0:nnx-1]
newx=newx[0:nnx-1]
if(keyword_set(verbose)) then print,'Sources left:',string(nnx,form='(i)')
return,newx
end
