function mage_create_fringe_pattern,flat_orders,nmin=nmin,med_w=med_w
    if(n_elements(nmin) ne 1) then nmin=8
    if(n_elements(med_w) ne 1) then med_w=3
    ymar=2
    s_f=size(flat_orders)
    fringes_orders=flat_orders*0.0

    for i=(nmin<(s_f[3]-1)),s_f[3]-1 do begin
        fringes_orders[*,*,i]=flat_orders[*,*,i]/mage_process_flat_order(flat_orders[*,*,i],/nopixvar)-1d
        if(ymar gt 0) then begin
            for y=0,ymar-1 do begin
                fringes_orders[*,y,i]=fringes_orders[*,ymar,i]
                fringes_orders[*,s_f[2]-y-1,i]=fringes_orders[*,s_f[2]-ymar-1,i]
            endfor
        endif
        if(med_w gt 1) then fringes_orders[*,*,i]=median(fringes_orders[*,*,i],med_w)
    endfor

    return,fringes_orders
end
