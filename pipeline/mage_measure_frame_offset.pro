function mage_measure_frame_offset,image1_in,image2_in,$
    pixel_fwhm=pixel_fwhm,sigma=sigma,smooth_sigma=smooth_sigma,$
    corners=corners,clean=clean,$
    xshift=xshift,yshift=yshift,magnification=magnification,int5p=int5p,$
    max_corr=max_corr,synframe=synframe,corr_mat=mtx,verbose=verbose

    image1=image1_in
    image2=image2_in
    s_im=size(image1)
    nx=s_im[1]
    ny=s_im[2]

    if(n_elements(smooth_sigma) ne 1) then smooth_sigma=0d
    if(n_elements(sigma) ne 1) then sigma=1.0d ;;; MagE PSF
    if(n_elements(magnification) ne 1) then magnification=10
    if(n_elements(xshift) ne 1) then xshift=10
    if(n_elements(yshift) ne 1) then yshift=10
    if(n_elements(corners) lt 1) then corners=[970,660,1120,770] ;; x0,y0,x1,y1 [[1230,1300,1400,1450],[460,1330,630,1480]]
    pixflat=1d

    s_c=size(corners)
    n_reg=(s_c[0] eq 1)? 1 : s_c[2]
    xy_off=dblarr(2,n_reg) + !values.d_nan

    for r=0,n_reg-1 do begin
        if(keyword_set(verbose)) then print,'Processing region (X1,Y1,X2,Y2): '+string(corners[*,r],format='(4i5)')
        if(keyword_set(clean)) then begin
            for im=0,1 do begin
                image=(im eq 0)? image1 : image2
                if(r eq 0) then image_med=median(image,3)
                min_med=min(image_med[corners[0,r]:corners[2,r],corners[1,r]:corners[3,r]],max=max_med,/nan)
                badpix=where(finite(image) ne 1 or $
                         image lt min_med-(max_med-min_med)*0.5 or $
                         image gt max_med+(max_med-min_med)*1.0, cbadpix)
                if(cbadpix gt 0) then image[badpix]=image_med[badpix]
                if(im eq 0) then image1=image else image2=image
            endfor
        endif

        p1=(image1/pixflat)[corners[0,r]:corners[2,r],corners[1,r]:corners[3,r]]
        bp1=where(finite(p1) ne 1, cbp1)
        if(cbp1 gt 1) then p1[bp1]=0d
        if(smooth_sigma gt 0.1d) then begin
            psf=psf_gaussian(ndim=2,npix=((smooth_sigma*5) > 15),/double,/norm,fwhm=2.355d*smooth_sigma)
            p1=convol(p1,psf)
        endif

        p2=(image2/pixflat)[corners[0,r]:corners[2,r],corners[1,r]:corners[3,r]]    
        bp2=where(finite(p2) ne 1, cbp2)
        if(cbp2 gt 1) then p2[bp2]=0d

        print,'Running image cross-correlation'
        mtx=correl_images_frac(p1,p2,xshift=xshift,yshift=yshift,mag=magnification,int5p=int5p)
        print,'Done'
        krnl=psf_gaussian(ndim=2,npix=((magnification*2.5) > 11),fwhm=double(magnification),/norm,/double)
        mtx=convol(mtx,krnl,/edge_truncate)
        corrmat_analyze,mtx,x_best,y_best,max_corr,print=verbose
        xy_off[*,r]=[x_best,y_best]/double(magnification)
    endfor
    return, xy_off
end
