function mage_xnl2y,x,n,l,trace_coeff,x0=x0,truen=truen

s_x = size(x)
s_n = size(n)
s_l = size(l)

if(n_elements(x0) ne 1) then x0=1024

if(array_equal(s_x[0:s_x[0]],s_n[0:s_x[0]]) ne 1 or $
   array_equal(s_x[0:s_x[0]],s_l[0:s_x[0]]) ne 1) then begin
    message,/inf,'X, N and L should have the same number of elements'
    return,-1
endif

n_ord=n_elements(trace_coeff[0,*,0])

n_inp=s_x[n_elements(s_x)-1]
x_vec=reform(x,n_inp)
n_vec=reform(n,n_inp)
if(keyword_set(truen)) then n_vec=19-n_vec
l_vec=reform(l,n_inp)

y_vec=dblarr(n_inp)+!values.d_nan

for i=0,n_ord-1 do begin
    g_vec=where(n_vec eq i,cg_vec)
    if(cg_vec eq 0) then continue
    order_borders = dblarr(cg_vec,2)
    order_borders[*,0]=poly(x_vec[g_vec]-x0,trace_coeff[*,i,0])
    order_borders[*,1]=poly(x_vec[g_vec]-x0,trace_coeff[*,i,1])
    y_vec[g_vec]=order_borders[*,0]+(order_borders[*,1]-order_borders[*,0])*l_vec[g_vec]
endfor

y=(s_x[0] le 1)? y_vec : reform(y_vec,s_x[1:s_x[0]])

return,y
end
