function mage_model_sc_light,image,trace_coeff,$
    deg1=deg1,deg2=deg2,margin=margin,smw=smw,$
    gain=gain,rdnoise=rdnoise,$
    n_iter=n_iter,bspline=bspline,bkspace=bkspace,fitdata=fitdata,max_deg=max_deg ;;;_Extra=extra_kw

s_im=size(image)
if(n_elements(n_iter) ne 1) then n_iter=3
if(n_elements(smw) ne 1) then smw=0
if(n_elements(margin) ne 1) then margin=(keyword_set(fitdata))? 0 : 2
if(n_elements(deg1) ne 1) then deg1=8
if(n_elements(deg2) ne 1) then deg2=(keyword_set(bspline))? 16 : 8 ;; 21 : 8
if(n_elements(bkspace) ne 1) then bkspace=256 ;; 20
if(n_elements(gain) ne 1) then gain=1.0
if(n_elements(rdnoise) ne 1) then rdnoise=1.0

if(keyword_set(bspline)) then n_iter=1

xcrd=dindgen(s_im[1]) # (dblarr(s_im[2])+1d)
ycrd=(dblarr(s_im[1])+1d) # dindgen(s_im[2])

n=mage_xy2nl(xcrd,ycrd,trace_coeff)
mask=n*0
mask[where(n ge 0 or finite(image) ne 1)]=1
if(keyword_set(fitdata)) then mask=1-mask
if(~keyword_set(bspline) and ~keyword_set(fitdata)) then mask[850:1200,320:475]=1

if(~keyword_set(fitdata)) then begin
    tr_20=trace_coeff[*,0,0]
    tr_20[0]-=65d
    yh=poly(dindgen(s_im[1])-1024d,tr_20)
    tr_20[0]-=42d
    yl=((poly(dindgen(s_im[1])-1024d,tr_20)) > 0)
    for i=0,s_im[1]-1 do if (yh[i] gt 0) then mask[i,yl[i]:yh[i]]=1

    tr_06=trace_coeff[*,n_elements(trace_coeff[0,*,0])-1,1]
    tr_06[0]+=15d +5d
    yl_l=poly(dindgen(s_im[1])-1024d,tr_06)
    yl_l=yl_l<(s_im[2]-1)
    tr_06[0]+=43d -12d
    yl_h=poly(dindgen(s_im[1])-1024d,tr_06)
    yl_h=yl_h<(s_im[2]-1)
    for i=0,s_im[1]-1 do mask[i,yl_l[i]:yl_h[i]]=1
endif

for y=-margin,margin do begin
    mask=mask+shift(mask,0,y)
endfor

overscan=(keyword_set(fitdata))? 0 : 2
if(overscan gt 0) then begin
    mask[0:overscan-1,*]=1
    mask[s_im[1]-overscan-1:*,*]=1
    mask[*,0:overscan-1]=1
    mask[*,s_im[2]-overscan-1:*]=1
endif

;sc_reg=where(mask eq 0 and image gt 0, csc_reg, compl=im_reg, ncompl=cim_reg)
sc_reg=where(mask eq 0 and image gt -1d2, csc_reg, compl=im_reg, ncompl=cim_reg)
if(csc_reg gt 0) then begin
    image_sm=image
    image_sm[im_reg]=!values.d_nan
    if(smw gt 1) then image_sm=median(image_sm,smw)
    d_arr=dblarr(3,csc_reg)
    d_arr[0,*]=reform(xcrd[sc_reg],1,csc_reg)
    d_arr[1,*]=reform(ycrd[sc_reg],1,csc_reg)
    d_arr[2,*]=reform(image_sm[sc_reg],1,csc_reg)
    pix_subs=lindgen(csc_reg)
    for iter=0,n_iter-1 do begin
        if(keyword_set(bspline)) then begin
            print,'Creating the '+(keyword_set(fitdata)? 'blaze' : 'scattered light')+' model using b-splines'
            s_x=sort(d_arr[0,*])
            xx=transpose(d_arr[0,s_x])
            yy=(transpose(d_arr[1,s_x])/512d)^2
            flux=transpose(d_arr[2,s_x])
            i_flux=gain^2 / (gain*abs(flux) + rdnoise^2)
            sset = bspline_iterfit(xx,sqrt(flux>0),invvar=i_flux/(flux>(rdnoise)),x2=yy,npoly=deg2,bkspace=bkspace) ;,_Extra=extra_kw)
        endif else begin
            f=sfit_2deg(d_arr[*,pix_subs],err=((transpose(d_arr[2,pix_subs])+rdnoise^2)>0.1)^0.5,deg1,deg2,kx=kx,/irreg,max_deg=max_deg)
            sig=robust_sigma(d_arr[2,pix_subs]-f)
            badpix=where(abs(d_arr[2,*]-poly2d(xcrd[sc_reg],ycrd[sc_reg],kx,deg1=deg1,deg2=deg2,/irre)) gt 3.0*sig,cbadpix,compl=pix_subs,ncompl=cpix_subs)
            print,'iter=',iter+1,' cpix_subs=',cpix_subs,' sig=',sig
        endelse
    endfor
    sc_model=(keyword_set(bspline))? $
        reform(bspline_valu(reform(xcrd,s_im[1]*s_im[2]),sset,x2=reform((ycrd/512d)^2,s_im[1]*s_im[2])),s_im[1],s_im[2])^2 : $
        poly2d(dindgen(s_im[1]),dindgen(s_im[2]),kx,deg1=deg1,deg2=deg2)
    sc_model = (sc_model > 0d)
endif else sc_model=image*0d

return, sc_model

end
